import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from 'lib/sequelize/db';
import bcrypt from 'bcryptjs';
import { hashPassword } from 'utils/passwordUtils';
import { url } from 'inspector';

const User = sequelize.define(
  'User',
  {
    surname: {
      type: DataTypes.STRING,
    },
    lastname: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
    },
    role: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    resetPasswordCode: {
      type: DataTypes.INTEGER(4),
    },
    resetPasswordCodeDate: {
      type: DataTypes.DATE,
    },
    isEmailValidated: {
      type: DataTypes.BOOLEAN,
    },
    validationEmailToken: {
      type: DataTypes.STRING,
    },
    participatedAt: {
      type: DataTypes.VIRTUAL
    },
  },
  {
    tableName: 'user',
  },
);

const Actor = sequelize.define(
  'Actor',
  {
    name: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    website: {
      type: DataTypes.STRING,
    },
    socialNetwork: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.FLOAT,
    },
    lng: {
      type: DataTypes.FLOAT,
    },
    shortDescription: {
      type: DataTypes.STRING,
    },
    isValidated: {
      type: DataTypes.BOOLEAN,
    },
    dateValidation: {
      type: DataTypes.DATE,
    },
    activity: {
      type: DataTypes.STRING,
    },
    volunteerDescription: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.STRING,
    },
    nbVolunteers: {
      type: DataTypes.VIRTUAL
    },
    gameParticipationDate: {
      type: DataTypes.VIRTUAL
    },
    siren: {
      type: DataTypes.STRING,
    },
    hasVideoVouaaar: {
      type: DataTypes.BOOLEAN,
    },
    enableOpenData: {
      type: DataTypes.BOOLEAN,
    },
    url: {
      type: DataTypes.STRING,
    },
    amapContratLegumes: {
      type: DataTypes.STRING,
    },
    amapAutresContrats: {
      type: DataTypes.STRING,
    },
    fermeModeDistribution: {
      type: DataTypes.STRING,
    },

    /*  location: {
            type: DataTypes.GEOMETRY('POINT'),
            allowNull: true
        },

       */
  },
  {
    tableName: 'actor',
  },
);

const Event = sequelize.define(
  'Event',
  {
    label: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.TEXT,
    },
    facebookUrl: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    startedAt: {
      type: DataTypes.DATE,
    },
    endedAt: {
      type: DataTypes.DATE,
    },
    published: {
      type: DataTypes.BOOLEAN,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.FLOAT,
    },
    lng: {
      type: DataTypes.FLOAT,
    },
    registerLink: {
      type: DataTypes.STRING,
    },
    practicalInfo: {
      type: DataTypes.STRING,
    },
    nbParticipants: {
      type: DataTypes.VIRTUAL
    },
    dateRule: {
      type: DataTypes.STRING,
    },
    limitPlace: {
      type: DataTypes.INTEGER,
    },
    url: {
      type: DataTypes.STRING,
    },
    participateButton: {
      type: DataTypes.BOOLEAN,
    }
  },
  {
    tableName: 'event',
  },
);

const Picture = sequelize.define(
  'picture',
  {
    label: {
      type: DataTypes.STRING,
      // unique: true,
    },
    originalPicturePath: {
      type: DataTypes.STRING,
      unique: true,
    },
    originalPictureFilename: {
      type: DataTypes.STRING,
      unique: true,
    },
    position: {
      type: DataTypes.INTEGER,
    },
    logo: {
      type: DataTypes.BOOLEAN,
    },
    main: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: 'picture',
  },
);

const Newsletter = sequelize.define(
  'Newsletter',
  {
    email: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: 'newsletter',
  },
);

const ActorEntries = sequelize.define(
  'actorEntries',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    linkDescription: DataTypes.STRING,
    topSEO: DataTypes.BOOLEAN
  }
);

const Entry = sequelize.define(
  'Entry',
  {
    code: DataTypes.STRING,
    label: DataTypes.STRING,
    value: DataTypes.STRING,
    icon: DataTypes.STRING,
    color: DataTypes.STRING,
    description: DataTypes.STRING,
    position: DataTypes.INTEGER,
    url: DataTypes.STRING,
    enabled: DataTypes.BOOLEAN,
  },
  {
    tableName: 'entries',
  },
);

const Collection = sequelize.define(
  'Collection',
  {
    code: DataTypes.STRING,
    label: DataTypes.STRING,
    multipleSelection: DataTypes.BOOLEAN,
    withDescription: DataTypes.BOOLEAN,
    value: DataTypes.STRING,
    position: DataTypes.INTEGER,
    enabled: DataTypes.BOOLEAN,
    actor: DataTypes.BOOLEAN,
    event: DataTypes.BOOLEAN,
    filter: DataTypes.BOOLEAN,
  },
  {
    tableName: 'collections',
  },
);

const OpeningHours = sequelize.define(
  'OpeningHour',
  {
    days: DataTypes.JSON,
    hours: DataTypes.JSON,
    place: DataTypes.JSON,
  },
  {
    tableName: 'opening_hours',
  },
);

const CodeControls = sequelize.define(
  'code_controls',
  {
    code: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.ENUM(['open', 'cancel', 'close']),
      allowNull: false,
    },
    action: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    info1: {
      type: DataTypes.STRING,
    },
    info2: {
      type: DataTypes.STRING,
    },
  }
);

const InviteActor = sequelize.define(
  'InviteActor',
  {
    requesterName: {
      type: DataTypes.STRING,
    },
    contactName: {
      type: DataTypes.STRING,
    },
    actorName: {
      type: DataTypes.STRING,
    },
    actorEmail: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    sendEmail: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: 'inviteActor',
  },
);

CodeControls.createCodeInstance = async (
  action,
  info1 = null,
  info2 = null,
) => {
  return await CodeControls.create({
    code: Math.floor(1000 + Math.random() * 9000),
    status: 'open',
    action,
    info1,
    info2,
  });
};

const Article = sequelize.define(
  'Article',
  {
    label: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.TEXT,
    },
    content: {
      type: DataTypes.TEXT('long'),
    },
    url: {
      type: DataTypes.STRING,
    },
    bannerPrincipalPicture: {
      type: DataTypes.BOOLEAN,
    },

  },
  {
    tableName: 'article',
  },
);

const RecipeTag = sequelize.define(
  'RecipeTag',
  {
    label: DataTypes.TEXT
  },
  {
    tableName: 'recipe_tag'
  }
);

const Recipe = sequelize.define(
  'Recipe',
  {
    category: {
      type: DataTypes.STRING,
    },
    label: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.TEXT,
    },
    content: {
      type: DataTypes.TEXT('long'),
    },
    time: {
      type: DataTypes.INTEGER,
    },
    nbPerson: {
      type: DataTypes.INTEGER,
    },
    type: {
      type: DataTypes.TEXT,
    },
    url: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: 'recipe',
  },
);

const IngredientBaseAlim = sequelize.define('IngredientBaseAlim', {
  produit: {
    type: DataTypes.STRING, // You can adjust the data type based on the actual type
    allowNull: false,
  },
  poids: {
    type: DataTypes.FLOAT, // Assuming the weight can be a decimal value
    allowNull: false,
  },
  energie: {
    type: DataTypes.FLOAT, // Assuming the energy can be a decimal value
    allowNull: false,
  },
  proteines: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  lipides: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  glucides: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  empreinteCarbone: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  agriculture: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  transformation: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  emballage: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  transport: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  distribution: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  poidsParUnite: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  densite: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  poidsParCuillereASoupe: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  poidsParCuillereACafe: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
},
  {
    tableName: 'base_alim_ingredient'
  }
);

const Ingredient = sequelize.define(
  'Ingredient',
  {
    name: {
      type: DataTypes.TEXT,
    },
    unit: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT('long'),
    },
    quantity: {
      type: DataTypes.FLOAT,
    }
  },
  {
    tableName: 'ingredient'
  }
);

/*const RecipeIngredient = sequelize.define(
  'RecipeIngredient',
  {
    quantity: {
      type: DataTypes.FLOAT,
    }
  },
  {
    tableName: 'recipe_ingredient'
  }
);
*/

const Resource = sequelize.define(
  'Resource',
  {
    type: {
      type: DataTypes.ENUM('ARTICLE', 'ARTICLE_OUAAA', 'RECIPE', 'ACTOR_VIDEO'),
    }
  },
  {
    tableName: 'resource',
  },
);



const startDB = () => {
  sequelize.sync();

  Event.belongsToMany(User, {
    through: 'user_event_participant',
    as: 'participants',
    foreignKey: 'event_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_participant',
    as: 'events',
    foreignKey: 'user_id',
  });

  Actor.belongsToMany(User, {
    through: 'user_actor_volunteer',
    as: 'volunteers',
    foreignKey: 'actor_id',
  });
  Actor.belongsToMany(User, {
    through: 'user_actor_game',
    as: 'gamers',
    foreignKey: 'actor_id',
  });
  Actor.belongsToMany(User, {
    through: 'user_actor_favorite',
    as: 'favorites',
    foreignKey: 'actor_id',
  });
  Actor.belongsToMany(Entry, {
    through: ActorEntries,
    as: 'entries',
    foreignKey: 'actor_id',
  });

  Entry.belongsToMany(Actor, {
    through: ActorEntries,
    as: 'actors',
    foreignKey: 'entry_id',
  });


  Event.belongsToMany(Entry, {
    through: 'event_entry',
    as: 'entries',
    foreignKey: 'event_id',
  });
  Event.belongsToMany(User, {
    through: 'user_event_favorite',
    as: 'favorites',
    foreignKey: 'event_id',
  });
  Entry.belongsToMany(Event, {
    through: 'event_entry',
    as: 'events',
    foreignKey: 'entry_id',
  });
  Entry.hasMany(Entry, { as: 'subEntries', foreignKey: 'entry_id' });
  Entry.belongsTo(Entry, { as: 'parentEntry', foreignKey: 'entry_id' });

  Event.hasMany(Event, { as: 'subEvents', foreignKey: 'event_id' });
  Event.belongsTo(Event, { as: 'parentEvent', foreignKey: 'event_id' });

  Entry.belongsTo(Collection, {
    as: 'collection',
    foreignKey: 'collection_id',
  });
  Collection.hasMany(Entry, { as: 'entries', foreignKey: 'collection_id' });

  User.belongsToMany(Actor, {
    through: 'user_actor_volunteer',
    as: 'actors',
    foreignKey: 'user_id',
  });

  Actor.belongsToMany(User, {
    through: 'user_actor_game',
    as: 'gameActors',
    foreignKey: 'actor_id',
  });
  User.belongsToMany(Actor, {
    through: 'user_actor_favorite',
    as: 'favoriteActors',
    foreignKey: 'user_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_favorite',
    as: 'favoriteEvents',
    foreignKey: 'user_id',
  });
  User.belongsToMany(Actor, {
    through: 'user_actor_admin',
    as: 'adminactors',
    foreignKey: 'user_id',
  });


  Actor.belongsToMany(Event, {
    through: 'actor_event',
    as: 'events',
    foreignKey: 'actor_id',
  });
  Event.belongsToMany(Actor, {
    through: 'actor_event',
    as: 'actors',
    foreignKey: 'event_id',
  });

  Event.belongsToMany(User, {
    through: 'user_event_admin',
    as: 'referents',
    foreignKey: 'event_id',
  });
  Actor.belongsToMany(User, {
    through: 'user_actor_admin',
    as: 'referents',
    foreignKey: 'actor_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_admin',
    as: 'adminevents',
    foreignKey: 'user_id',
  });

  Actor.belongsToMany(Article, {
    through: 'actor_article',
    as: 'articles',
    foreignKey: 'actor_id',
  });
  Article.belongsToMany(Actor, {
    through: 'actor_article',
    as: 'actors',
    foreignKey: 'article_id',
  });

  Newsletter.belongsTo(User, { foreignKey: 'user_id' });
  User.hasOne(Newsletter, { foreignKey: 'user_id' });

  Newsletter.belongsTo(Event, { foreignKey: 'event_id' });
  Event.hasOne(Newsletter, { foreignKey: 'event_id' });

  Newsletter.belongsTo(Actor, { foreignKey: 'actor_id' });
  Actor.hasOne(Newsletter, { foreignKey: 'actor_id' });

  Picture.belongsTo(Actor, { foreignKey: 'actor_id' });
  Actor.hasMany(Picture, { foreignKey: 'actor_id', as: 'pictures' });

  Picture.belongsTo(Event, { foreignKey: 'event_id' });
  Event.hasMany(Picture, { foreignKey: 'event_id', as: 'pictures' });

  Picture.belongsTo(Article, { foreignKey: 'article_id' });
  Article.hasMany(Picture, { foreignKey: 'article_id', as: 'pictures' });

  Actor.belongsTo(User, {
    foreignKey: 'validated_actor_id',
    as: 'userValidated',
  });
  User.hasMany(Actor, { foreignKey: 'validated_actor_id', as: 'validatedActors' });

  Actor.belongsTo(User, {
    foreignKey: 'contact_id',
    as: 'userContact',
  });
  User.hasMany(Actor, { foreignKey: 'contact_id', as: 'contactActors' });

  OpeningHours.belongsTo(Actor, {
    as: 'actor',
    foreignKey: 'actorId',
  });
  Actor.hasMany(OpeningHours, { as: 'openingHours', foreignKey: 'actorId' });

  Picture.belongsTo(Recipe, { foreignKey: 'recipe_id' });
  Recipe.hasMany(Picture, { foreignKey: 'recipe_id', as: 'pictures' });

  Recipe.belongsTo(Actor, { foreignKey: 'actor_id', as: 'actor' });
  Actor.hasOne(Newsletter, { foreignKey: 'actor_id' });

  Recipe.belongsTo(User, { foreignKey: 'user_id', as: 'user' });
  User.hasMany(Recipe, { foreignKey: 'user_id', as: 'recipes' });
  Actor.hasMany(Recipe, { foreignKey: 'actor_id', as: 'recipes' });
  
  Recipe.belongsToMany(RecipeTag, { through: 'recipe_has_tag', as: 'Tags' });
  RecipeTag.belongsToMany(Recipe, { through: 'recipe_has_tag', as: 'Recipes' });

 /* Recipe.belongsToMany(Ingredient, {
    through: {
      model: RecipeIngredient,
    },
    foreignKey: 'ingredientId',
    as: 'Ingredients'
  });
  Ingredient.belongsToMany(Recipe, {
    through: {
      model: RecipeIngredient,
    },
    foreignKey: 'recipeId',
    as: 'Recipes',
  });
*/
  Resource.belongsTo(Article, { foreignKey: 'article_id', as: 'article' });
  Resource.belongsTo(Recipe, { foreignKey: 'recipe_id', as: 'recipe' });
  Resource.belongsTo(Actor, { foreignKey: 'actor_id', as: 'actor' });


  Actor.belongsToMany(Actor, {
    through: 'actor_memberof_actor',
    as: 'members',
    foreignKey: 'actor_id',
  });

  Actor.belongsToMany(Actor, {
    through: 'actor_memberof_actor',
    as: 'memberOf',
    foreignKey: 'actormemberof_id',
  });

  Actor.hasMany(Actor, { as: 'proposedActor', foreignKey: 'referencing_actor_id' });
  Actor.belongsTo(Actor, { as: 'referencingActor', foreignKey: 'referencing_actor_id' });
  IngredientBaseAlim.hasOne(Ingredient, { foreignKey: 'baseAlimIngredientId' });
  Ingredient.belongsTo(IngredientBaseAlim, { foreignKey: 'baseAlimIngredientId' });
  Ingredient.belongsTo(Recipe, {  as: 'recipe', foreignKey: 'recipe_id' });
  Recipe.hasMany(Ingredient, { as: 'ingredients', foreignKey: 'recipe_id' });
 // Ingredient.hasOne(IngredientBaseAlim, { foreignKey: 'base_alim_ingredient_id' });
};

export {
  User,
  CodeControls,
  Actor,
  Event,
  Newsletter,
  Picture,
  Entry,
  Collection,
  ActorEntries,
  OpeningHours,
  InviteActor,
  Article,
  Recipe,
  Resource,
  Ingredient,
  IngredientBaseAlim
};

export default startDB;
