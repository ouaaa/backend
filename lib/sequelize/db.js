import Sequelize from 'sequelize';
import path from 'path';

import { development as devConnection, production } from './connection';

let database;
const { Op } = Sequelize;
const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col,
};


switch (process.env.NODE_ENV) {
  case 'production':
    database = new Sequelize(
      production.database,
      production.username,
      production.password,
      {
        host: production.host,
        port: production.port,
        logging:true,
        dialect: 'mysql',
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
        operatorsAliases,
      },
    );
    break;
  default:
    database = new Sequelize(
      devConnection.database,
      devConnection.username,
      devConnection.password,
      {
        host: devConnection.host,
        port: devConnection.port,
        dialect: devConnection.dialect,
        logging: true,
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
        //    storage: path.join(process.cwd(), 'db', 'database.sqlite'),
        operatorsAliases,
      },
    );
}

export default database;
