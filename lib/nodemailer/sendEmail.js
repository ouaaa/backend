import { emailTransporter } from 'pages/api/graphql';
var mailerhbs = require('nodemailer-express-handlebars');
import nodemailer from 'nodemailer';
import config from '../../config.json';
export const sendEmail = async (
  to,
  emailTitle,
  template,
  data,
  helpers = undefined,
  attachments = undefined,
  callback = null,
  cc = null
) => {
  var emailTransporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    secureConnection: process.env.EMAIL_SECURECONECTION === 'true' ? true : false,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });
  data.clientUrl=process.env.CLIENT_URL;
  const mailOptions = {
    from: {
      name: process.env.EMAIL_FROM,
      address: process.env.EMAIL_USER,
    }, // sender address
    to: to, // list of receivers
    cc: cc, // add email in copy (cc)
    subject: `OUAAA - ${emailTitle}`,
    template: `${template}`, //Name email file template
    context: data,
    attachments
  };
  emailTransporter.use(
    'compile',
    mailerhbs({
      viewEngine: {
        extName: '.hbs',
        partialsDir: './emailTemplates',
        layoutsDir: './emailTemplates',
        defaultLayout: `${template}.hbs`,
        helpers
      },
      viewPath: './emailTemplates', //Path to email template folder
      extName: '.hbs', //extension of email template
    }),
  );

  let result = null;
 result = await emailTransporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
     // console.log(`Message sent: ${info.response}`);
      //console.log(mailOptions);
    }
    if (callback) {
      callback(error, info);
    }
  }); 

  return result;
};
