import { join } from 'node:path'
const { printSchema } = require("graphql");
import { loadSchemaSync } from '@graphql-tools/load'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader'
import { addResolversToSchema } from '@graphql-tools/schema'
export const typeDefs  = loadSchemaSync( 'lib/apolloServer/schema.graphql', { loaders: [new GraphQLFileLoader()] })
