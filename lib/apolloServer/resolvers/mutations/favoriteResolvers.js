import {
  Actor,
  Event
} from 'lib/sequelize/models';

const favoriteResolver = {
    Mutation: {
        addFavorite: async(parentValue, args, context) => {
          if(args.actorId != null){
          try {
            const actor = await Actor.findOne({ where: { id: args.actorId } });

          if(args.favorite){
            await actor.addFavorites(args.userId);
          }else{
            await actor.removeFavorites(args.userId);
          }
           
            return true;
          } catch (error) {
            console.log(error);
            throw error;
          }
        }else{
            try {
              const event = await Event.findOne({ where: { id: args.eventId } });
              
            if(args.favorite){
              await event.addFavorites(args.userId);
            }else{
              await event.removeFavorites(args.userId);
            }
             
              return true;
            } catch (error) {
              console.log(error);
              throw error;
            }
        }
      },

     
    }
}

export default favoriteResolver;