import { ForbiddenError } from '@apollo/server';
import { Collection, User, Entry } from 'lib/sequelize/models';
import { DataTypes } from 'sequelize';
const { Op } = require('sequelize');

const collectionResolvers = {
  Query: {
    collections: async (parent, args, context, info) => {
      const collections = await Collection.findAll({
        order: [['position', 'ASC'],[{model: Entry, as: 'entries'},'position', 'ASC'],[{model: Entry, as: 'entries'},{model: Entry, as: 'subEntries'},'position', 'ASC']],
        include: {
          order: [['position', 'ASC']],
          model: Entry,
          as: 'entries',
          attributes: ['id', 'label', 'position','icon','color','description'],
          include: {
            order: [['position', 'ASC']],
            model: Entry,
            as: 'subEntries',
            attributes: ['id', 'label', 'position','icon','color','description'],
            include: {
              order: [['position', 'ASC']],
              model: Entry,
              as: 'subEntries',
              attributes: ['id', 'label', 'position','icon','color','description'],
              
            },
          },
        },
      });

      return collections;
    },
    category: async (parent, args, context, info) => {
      const collection = await Collection.findAll({
        where: {
          label: 'Sujets'
        },
        include: {
          model: Entry,
          as: 'entries',
          include: {
            model: Entry,
            as: 'subEntries',
            where: {
              id: args.id
            }
          },
        },
      });

      if (collection.length > 0 && collection[0].entries.length > 0 && collection[0].entries[0].subEntries.length > 0) {
        return collection[0].entries[0].subEntries[0];
      } else {
        console.log(`No category found with id: ${args.id}`);
        return {};
      }
    },
    entry: async (parent, args, context, info) => {

      // Ensure args.id and args.code are not undefined or null
    if (!args.id && !args.code) {
      throw new Error("Either 'id' or 'code' must be provided.");
    }
    const entry = await Entry.findOne({
      where: {
        [Op.or]: [
          args.id ? { id: args.id } : null,   // Only add id to the query if it's provided
          args.code ? { code: args.code } : null // Only add code to the query if it's provided
        ].filter(Boolean) // Filter out null values from the array
      }
    });
    return entry;
    },
    categories: async (parent, args, context, info) => {
      const collections = await Collection.findAll({
        where: {
          label: 'Sujets'
        },
        order: [['position', 'ASC'],[{model: Entry, as: 'entries'},'position', 'ASC'],[{model: Entry, as: 'entries'},{model: Entry, as: 'subEntries'},'position', 'ASC']],
        include: {
          order: [['position', 'ASC']],
          model: Entry,
          as: 'entries',
          attributes: ['id', 'label', 'position','icon','color','description'],
          include: {
            order: [['position', 'ASC']],
            model: Entry,
            as: 'subEntries',
            attributes: ['id', 'label', 'position','icon','color','description'],
          },
        },
      });

      const categories = [];

      if (collections?.length > 0) {
        for (const entry of collections[0].entries) {
          for (const subEntry of entry.subEntries) {
            subEntry.color = entry.color
            categories.push(subEntry)
          }
        }
      }
      return categories;
    },
  },
  Mutation: {
    /* mutation to update user's infos */
  },
  Collection: {
    /* collecton's author */
  },
};
export default collectionResolvers;
