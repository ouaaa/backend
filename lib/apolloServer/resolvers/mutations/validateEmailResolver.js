import { User } from 'lib/sequelize/models';
import validateEmailFormat from 'utils/validateEmailFormat';
import { verifyAuthentification } from 'services/auth.service';
import { EMAIL_VALIDATION_AUTH } from 'config/constants/constants';

const validateEmailResolver = {
  Mutation: {
    validateEmail: async (parentValue, args, context) => {
      if (verifyAuthentification(context, EMAIL_VALIDATION_AUTH)) {
        if (!args.email || !args.token)
          throw new Error('Bad information (email or token)');

        if (!validateEmailFormat(args.email))
          throw new Error('Email format is wrong');

        let user = null;

        try {
          user = await User.findOne({
            where: {
              email: args.email,
              validationEmailToken: args.token,
            },
          });
        } catch (err) {
          console.log(err);
          throw new Error('Global mutation error');
        }

        if (!user) throw new Error('Cannot confirm this email address');
        else {
          if (user.isEmailValidated)
            throw new Error('User email address is already validated');
          else {
            await user.update({ isEmailValidated: true });
            return true;
          }
        }
      }
    },
  },
};

export default validateEmailResolver;
