import { Op } from 'sequelize';
import {
  Article,
  Resource,
  Actor,
  Recipe,
  Picture,
} from 'lib/sequelize/models';

const resourceResolvers = {
  Query: {
    resources: async (parent, args, context, info) => {
      let whereCondition = {};
      const resourceType = args.type || 'article';
      if (args.search) {
        if (resourceType === 'video_actor') {
          whereCondition = {
            where: {
                name: {
                  [Op.like]: `%${args.search}%`
                },
            }
          }
       
        }else{
          whereCondition = {
            where: {
              [Op.or]: {
                label: {
                  [Op.like]: `%${args.search}%`
                },
                shortDescription: {
                  [Op.like]: `%${args.search}%`
                },
              }
            }
          }
        }
      }

      const includes = [];

      const asArticle = {
        model: Article,
        as: 'article',
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          }
        ],
        ...whereCondition,
      }

      const asRecipe = {
        model: Recipe,
        as: 'recipe',
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          }
        ],
        ...whereCondition,
      }
      const asActor = {
        model: Actor,
        as: 'actor',
        ...whereCondition,
      }
      if (resourceType === 'recipe') {
        includes.push(asRecipe);
      } else if (resourceType === 'article') {
        includes.push(asArticle);
      } else if (resourceType === 'video_actor') {
        includes.push(asActor);
      }

      
      const resources = await Resource.findAll({
        include: includes
      });
      return (resources.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1)) || []);
    }
  },
  Mutation: {

  },
  Resource: {
    /* collecton's author */
  },
};

export default resourceResolvers;