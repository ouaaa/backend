
import {
  Actor,
  Event,
  User,
  Picture,
  Entry,
  Collection,
  ActorEntries,
  Article,
  OpeningHours
} from 'lib/sequelize/models';
import { DataTypes, Op } from 'sequelize';
import randToken from 'rand-token';
import fs from 'fs';
import { sendEmail } from 'lib/nodemailer/sendEmail';
import moment from 'moment';
import e from 'express';
import {transformNameToUrl} from 'utils/urlUtils';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { isAuthenticated } from '../middleware';

const Sequelize = require('sequelize');
const moveNewPictureToActor = async (filename, id) => {
  let dirpath = `./public/static/images/actor/${id}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/actor/${id}/${filename}`;
  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/` + filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path);
  })

  return {
    url,
    filename,
    path,
  };


};


const actorResolvers = {
  Query: {
    actors: async (parent, args, context, info) => {
      const Sequelize = require('sequelize');

      let actors;
      var whereCondition = {};
      var includeCondition = {};
      if (args.postCode != undefined) {
        whereCondition['postCode'] = args.postCode;
      }
      if(!(args.canAdmin!=undefined && args.canAdmin==true)){
        whereCondition['isValidated'] = true;
      }
      if (args.search != undefined) {
        whereCondition = {
          ...whereCondition, ...{
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('activity')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
            ]
          }
        };
      }

      if (args.favoritesForUser != undefined && args.favoritesForUser != null) {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          where: {
            id: args.favoritesForUser,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          through: {
            attributes: [],
          },
        };
      }

      if (args.actorsCollective != undefined && args.actorsCollective.length != 0) {
        includeCondition = {
          model: Actor,
          as: 'memberOf',
          attributes: ['id'],
          where: {
            id: {
              [Op.in]: args.actorsCollective,
            },
          },
          through: {
            attributes: [],
          },
        };
      }
  
      
      var allEntries = [];
      if (args.entries != undefined && args.entries.length != 0) {
        args.entries.forEach((entries) => {
          if (entries != undefined) {
            entries.forEach((entry) => {
              allEntries.push(entry);
            });
          }
        });
      }
      if (allEntries.length != 0) {

        let actorsBeforeFiltering;

        actorsBeforeFiltering = await Actor.findAll({
          where: whereCondition,
          include: [
            includeCondition,
            {
              model: Entry,
              as: 'entries',
              attributes: ['id', 'code', 'label', 'icon', 'description', 'color'],
              where: {
                id: {
                  $or: allEntries,
                },
              },
              through: {
                attributes: ['entry_id', 'actor_id'],
              },
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position', 'color'],
                },
              ]
            },
            {
              model: User,
              as: 'userValidated',
              attributes: ['id', 'surname', 'lastname'],
    
            },
          
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },

          ],
        });
        var actorFilterd = [];

        actorsBeforeFiltering.forEach(async (actor) => {
          let isOk = true;
          args.entries.forEach((entries) => {
            if (entries != undefined) {
              let isContained = false;
              actor.entries.forEach((actorEntry) => {
                if (entries.includes('' + actorEntry.id)) {
                  isContained = true;
                }
              });

              if (entries.length != 0 && !isContained) {
                isOk = false;
              }
            }
          });
          if (isOk) {
            actorFilterd.push(actor);
          }
        });

        actors = actorFilterd;
      } else {
        actors = await Actor.findAll({
          where: whereCondition,
          order: [
            [args.sort ? args.sort : 'name', args.way ? args.way : 'ASC'],
          ],
          limit: args.limit,
          include: [
            includeCondition,
            {
              model: Entry,
              as: 'entries',
              attributes: ['id', 'code', 'label', 'icon', 'description', 'color'],
              through: {
                attributes: ['entry_id', 'actor_id'],
              },
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position', 'color'],
                },
              ]
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              model: User,
              as: 'userValidated',
              attributes: ['id', 'surname', 'lastname'],
    
            },
            {
              model: User,
              as: 'favorites',
              attributes: ['id'],
              through: {
                attributes: [],
              },
            },
          ],
        });
      }

      
      if (args.canAdmin) {
        if (context.req?.session?.user?.id && context.req?.session?.user?.role === 'admin') {
          return actors;
        } else if (context.req?.session?.user?.id) {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Actor,
                as: 'adminactors',
              },
            ]
          });

          const u = user.get({ plain: true});
          const userActors = u.adminactors.map(s => s.id);

          return actors
            .map(actor => actor.get({ plain: true}))
            .filter(actor => userActors.includes(actor.id));
        } else {
          return [];
        }
      }

      return actors;
    },
    actorsByCategory: async (parent, args, context, info) => {
      const actors = Actor.findAll({
        where: {
          isValidated: true
        },
        include: [
          {
            model: Entry,
            as: 'entries',
            where: {
              id: args.categoryId
            },
            through: {
              attributes: ['entry_id', 'actor_id'],
            },
            include: [
              {
                model: Entry,
                as: 'parentEntry',
              },
            ]
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'logo'
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
        ]
      })
      return actors
    },
    actorsByEntry: async (parent, args, context, info) => {
      const actors = await Actor.findAll({
        where: {
          isValidated: true
        },
        include: [
          {
            model: Entry,
            as: 'entries',
            where: {
              id: args.entryId
            },
            through: {
              attributes: ['entry_id', 'actor_id'],
            }
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'logo'
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
        ]
      })
      return actors
    },
    actorsAdmin: async (parent, args, context, info) => {
      let actors;
      if (context?.req?.session?.user) {
        let params = {
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id', 'actor_id'],
              },
            },
          ],
        };

        params.attributes = {
          include: [
            [
              Sequelize.literal(`(SELECT COUNT(*) FROM user_actor_volunteer uav WHERE uav.actor_id = Actor.id)`),
              'nbVolunteers'
            ]
          ]
        };

        if (context?.req?.session?.user.role == 'admin' || context?.req?.session?.user.role == 'acteurAdminRole') {
          params.include.push({
            model: User,
            as: 'userValidated',
            attributes: ['id', 'surname', 'lastname'],

          });
        } else {
          params.include[0].where = { id: args.userId };
        }

        actors = await Actor.findAll(params, {
          orderBy: [
            ['createdAt', 'DESC'],
          ]
        });

        actors = actors.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1));
      }

      return actors;
    },
    actorsCollective: async (parent, args, context, info) => {
      let actors;
      actors = await Actor.findAll({
        include: [
          {
            model: Actor,
            as: 'members',
            attributes: ['id'],
          }
        ],
        where: {
          '$members.id$': {
            [Op.gt]: 0
          }
        },
        orderBy: [
          ['createdAt', 'DESC'],
        ]
      });

      actors = actors.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1));

      return actors;
    },
    userActorGame: async (parent, args, context, info) => {
      let actors;
      if (context?.req?.session?.user) {
        let params = {
          include: [
            {
              model: User,
              as: 'gamers',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id', 'actor_id'],
              },
            },
          ],
        };

        params.attributes = {
          include: [
            [
              Sequelize.literal(`(SELECT createdAt FROM user_actor_game uag WHERE uag.actor_id = Actor.id and uag.UserId = ` + args.userId + `)`),
              'gameParticipationDate'
            ]
          ]
        };


        params.include[0].where = { id: args.userId };

        actors = await Actor.findAll(params);
      }

      return actors;
    },
    actor: async (parent, args, context, info) => {
      const startQuery = moment();
      var whereCondition = {};
      if (args.id != undefined) {
        whereCondition['id'] = args.id;
      } else if (args.url != undefined) {
        if (args.url != undefined) {
          whereCondition['url'] = args.url;
        }
      }
    
      const actor = await Actor.findOne({
        where: whereCondition,
        include: [
          {
            model: Entry,
            as: 'entries',
            include: [
              {
                model: Entry,

                as: 'parentEntry',
                attributes: ['id', 'label', 'position'],
                include: {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
                include: [
                  {
                    model: Entry,
    
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                    
    
                  },
                  {
                    model: Collection,
    
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },
                ],

              },
              {
                model: Collection,

                as: 'collection',
                attributes: ['id', 'code', 'label', 'position'],
              },
            ],
            order: [{ model: ActorEntries }, 'topSEO', 'ASC'],
          },
          {
            model: User,
            as: 'volunteers',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          },

          {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'logo',
              'main',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            separate: true,
            model: OpeningHours,
            as: 'openingHours',
            attributes: ['id', 'days', 'hours', 'place'],
          },
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
          {
            model: Actor,
            as: 'memberOf',
            attributes: ['id', 'name'],
            include: [{
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'logo'
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
              }
            ],
            through: {
              attributes: [],
            },
          },
          {
            model: Actor,
            as: 'members',
            attributes: ['id', 'name','lat','lng','address','postCode','city','phone','email','website','socialNetwork','activity','description','shortDescription','volunteerDescription','siren','enableOpenData','contact_id','isValidated','url'],
            include: [{
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'logo'
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
              }
            ]
          },
          {
            model: Actor,
            as: 'referencingActor',
            attributes: ['id', 'name'],

          }
        ],
      });
      const endQuery = moment();
      //console.log( "query in " + endQuery.diff(startQuery,'seconds')+'s');
      return actor;
    },
    volunteers: async (parent, args, context, info) => {
      let volunteers = [];

      if (context?.req?.session?.user) {
        volunteers = User.findAll({
          include: [
            {
              model: Actor,
              as: 'actors',
              where: {
                id: args.actorId
              }
            },
          ],
          attributes: {
            include: [
              [
                Sequelize.literal(
                  `(SELECT createdAt FROM user_actor_volunteer uev WHERE uev.actor_id = ${args.actorId} AND uev.user_id = User.id)`
                ),
                'participatedAt'
              ]
            ]
          }
        });
      }

      return volunteers;
    },
  },
  Mutation: {
    /* mutation to create user's infos */
    createActor: async (parent, args, context, info) => {
      try {
        //   if (!context.session.user)
        const user = await User.findByPk(args.userId);
        const actorCreated = await Actor.create({
          name: args.actorInfos.name,
          url: transformNameToUrl(args.actorInfos.name),
          email: args.actorInfos.email,
          phone: args.actorInfos.phone,
          address: args.actorInfos.address,
          postCode: args.actorInfos.postCode,
          city: args.actorInfos.city,
          website: args.actorInfos.website,
          socialNetwork: args.actorInfos.socialNetwork,
          activity: args.actorInfos.activity,
          description: args.description,
          shortDescription: args.actorInfos.shortDescription,
          lat: args.actorInfos.lat,
          lng: args.actorInfos.lng,
          volunteerDescription: args.volunteerDescription,
          siren: args.actorInfos.siren,
          referencing_actor_id: args.actorInfos.referencingActor,
          amapContratLegumes: args.actorInfos.amapContratLegumes,
          amapAutresContrats: args.actorInfos.amapAutresContrats,
          fermeModeDistribution: args.actorInfos.fermeModeDistribution,
        });

        let entriesWithInformationAlreadeyCreated = [];
        if (args.actorInfos.entriesWithInformation) {
          entriesWithInformationAlreadeyCreated = args.actorInfos.entriesWithInformation.map(entryWithInformation => { return entryWithInformation.entryId })
          const resultAddEntriesInformation = args.actorInfos.entriesWithInformation.map(async (entryWithInformation) => {
            actorCreated.ActorEntries = {
              topSEO: entryWithInformation.topSEO,
              linkDescription: entryWithInformation.linkDescription
            }
            await actorCreated.addEntry(entryWithInformation.entryId, { through: { topSEO: entryWithInformation.topSEO, linkDescription: entryWithInformation.linkDescription } })
            entriesWithInformationAlreadeyCreated.push(entryWithInformation.entryId);

          });
          Promise.all(resultAddEntriesInformation);
        }



        if (args.actorInfos.entries) {
          args.actorInfos.entries.forEach(async (entry) => {
            if (parseInt(entry) > 0) {
              if (!entriesWithInformationAlreadeyCreated.includes(entry)) {
                await actorCreated.addEntry(entry);
              }
            }
          });
        }

        if (args.actorInfos.contactId) {
          actorCreated.contact_id = args.actorInfos.contactId;
        } else {
          actorCreated.contact_id = args.userId;
        }

        if (args.actorInfos.enableOpenData != undefined) {
          actorCreated.enableOpenData = args.actorInfos.enableOpenData;
        }
        await actorCreated.save();

        await actorCreated.addReferent(user.id);
        // await actorCreated.save();
        const result = await Actor.findOne({
          where: { id: actorCreated.id },
          include: [
            {
              model: Actor,
              as: 'memberOf',
              attributes: ['id'],
            },
            {
              model: Entry,
              as: 'entries',
              attributes: ['id'],
            },
            {
              separate: true,
              model: OpeningHours,
              as: 'openingHours',
              attributes: ['id', 'days', 'hours', 'place'],
            },
            {
              model: User,
              as: 'referents',
              attributes: ['id'],
            },
          
          ],
        });

        if (args.actorInfos.referents) {
          var existingReferents = [];
          result.referents.forEach(async (referent) => {
            existingReferents.push(referent.id);

            if (!args.actorInfos.referents.includes(referent.id.toString())) {
              await result.removeReferent(referent);
            }
          });

          args.actorInfos.referents.forEach(async (referentId) => {
            if (!existingReferents.includes(parseInt(referentId))) {
              await result.addReferent(referentId);
              const referent = await User.findByPk(referentId);
              const emailTitle = "Vous êtes référent de " + result.name + " sur OUAAA!";

              const emailTemplate = 'addReferentActor';

              const data = {
                nomActeur: result.name,
                idActor: result.id,
                nomReferent: referent.surname,
                urlActorArea: process.env.CLIENT_URL + "/actorAdmin"
              };
              await sendEmail(referent.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);
            }
          });
        }

        if (args.actorInfos.memberOf) {
          var existingMemberOf = [];
          result.memberOf.forEach(async (memberOf) => {
            existingMemberOf.push(memberOf.id);

            if (!args.actorInfos.memberOf.includes(memberOf.id.toString())) {
              await result.removeMemberOf(memberOf);
            }
          });

          args.actorInfos.memberOf.forEach(async (memberOfId) => {
            if (!existingMemberOf.includes(parseInt(memberOfId))) {
              await result.addMemberOf(memberOfId);
            }
          });
        }

        if (args.openingHours) {
          args.openingHours.forEach(async (openingHourdto) => {
            const openingHour = await OpeningHours.create({
              days: openingHourdto.days,
              hours: openingHourdto.hours,
              place: openingHourdto.place,
            });
            await openingHour.setActor(result.id);
          });


          actorCreated.contact_id = args.actorInfos.contactId;
        }
        let emailTitle =
          "Bienvenue en tant qu'acteur de la transition sur Ouaaa";

        let emailTemplate = 'actorCreationEmail';
        let nomActeurReferenceur ="";
   
        if(args.actorInfos.referencingActor){
          emailTemplate = 'actorCreationEmailRefercingActor';
          emailTitle = "Invitation à rejoindre la plateforme OUAAA! - Valorisons ensemble votre engagement pour la Transition en Aunis";

          const referenceur = await Actor.findOne({
            where: { id: args.actorInfos.referencingActor }
          });
          nomActeurReferenceur = referenceur.name;

        }
        const data = {
          nomActeur: result.name,
          email: result.email,
          idActor: result.id,
          nomActeurReferenceur: nomActeurReferenceur,
          url: process.env.CLIENT_URL + "/admin/actors/" + result.id + "?addAsReferent=true",
          urlCharte: process.env.CLIENT_URL + "/charter",
        };

        await sendEmail(result.email + ',' + user.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);

        if (args.logoPictures) {
          const pictures = await managePicture(
            args.logoPictures,
            actorCreated.id,
          );
        }

        if (args.mainPictures) {
          const pictures = await managePicture(
            args.mainPictures,
            actorCreated.id,
          );
        }
        if (args.pictures) {
          const pictures = await managePicture(args.pictures, actorCreated.id);
        }

        return result;
      } catch (err) {
        console.log(err);
      }

      return result;
    },


    editActor: async (parent, args, context, info) => {

        //TODO remove after all events are updated
        const updateUrl = await Entry.findOne({ where: { code: 'updateUrlActor' } });
        if(updateUrl){
          const actors = await Actor.findAll();
          actors.forEach(async (actor) => {
            await actor.update({
              url: transformToUrl(actor.name)
            });
        })
        updateUrl.destroy();
        }
      try {
        const actorEdited = await Actor.update(
          {
            name: args.actorInfos.name,
            email: args.actorInfos.email,
            url: transformNameToUrl(args.actorInfos.name),
            phone: args.actorInfos.phone,
            address: args.actorInfos.address,
            postCode: args.actorInfos.postCode,
            city: args.actorInfos.city,
            website: args.actorInfos.website,
            socialNetwork: args.actorInfos.socialNetwork,
            activity: args.actorInfos.activity,
            description: args.description,
            lat: args.actorInfos.lat,
            lng: args.actorInfos.lng,
            volunteerDescription: args.volunteerDescription,
            shortDescription: args.actorInfos.shortDescription,
            siren: args.actorInfos.siren,
            amapContratLegumes: args.actorInfos.amapContratLegumes,
            amapAutresContrats: args.actorInfos.amapAutresContrats,
            fermeModeDistribution: args.actorInfos.fermeModeDistribution,
          },
          {
            where: {
              id: args.actorId,
            },
          },
        );
        if(args.actorInfos.removeReferencingActor){
          await Actor.update(
            {
              referencing_actor_id: null
            },
            {
              where: {
                id: args.actorId,
              },
            },
          );
        }

        const result = await Actor.findOne({
          where: { id: args.actorId },
          include: [
            {
              model: Entry,
              as: 'entries',
              attributes: ['id'],
            },
            {
              separate: true,
              model: OpeningHours,
              as: 'openingHours',
              attributes: ['id', 'days', 'hours', 'place'],
            },
            {
              model: User,
              as: 'referents',
              attributes: ['id'],
            },
            {
              model: Actor,
              as: 'memberOf',
              attributes: ['id'],
            },
          ],
        });
        if (args.actorInfos.hasVideoVouaaar != undefined) {
          result.hasVideoVouaaar = args.actorInfos.hasVideoVouaaar;
        }
        if (args.actorInfos.enableOpenData != undefined) {
          result.enableOpenData = args.actorInfos.enableOpenData;
        }
        if (args.actorInfos.contactId) {
          result.contact_id = args.actorInfos.contactId;
        } else {
          result.contact_id = args.userId;
        }
        await result.save();


        if (args.openingHours) {
          await result.openingHours.forEach(async (openingHourdto) => {
            await openingHourdto.destroy();
          });
          var openingHourUpdated = [];
          await args.openingHours.forEach(async (openingHourdto) => {
            const openingHour = await OpeningHours.create({
              days: openingHourdto.days,
              hours: openingHourdto.hours,
              place: openingHourdto.place ? openingHourdto.place : '',
            });
            await openingHour.setActor(result.id);
            openingHourUpdated.push(openingHour);
          });

        }

        let entriesWithInformationAlreadeyCreated = [];
        if (args.actorInfos.entriesWithInformation) {
          entriesWithInformationAlreadeyCreated = args.actorInfos.entriesWithInformation.map(entryWithInformation => { return parseInt(entryWithInformation.entryId) })

          var existingEntries = [];
          result.entries.forEach(async (entry) => {
            existingEntries.push(entry.id);
          });


          args.actorInfos.entriesWithInformation.forEach(async (entryWithInformation) => {
            if (!existingEntries.includes(parseInt(entryWithInformation.entryId))) {
              await result.addEntry(entryWithInformation.entryId, { through: { topSEO: entryWithInformation.topSEO, linkDescription: (entryWithInformation.linkDescription != undefined ? entryWithInformation.linkDescription : null) } })
            } else {
              let resultAddEntries = result.entries.forEach(async (entry) => {
                if (entry.id === parseInt(entryWithInformation.entryId, 10)) {
                  entry.actorEntries.linkDescription = entryWithInformation.linkDescription;
                  entry.actorEntries.topSEO = entryWithInformation.topSEO;
                  await entry.actorEntries.save();
                }

              });
              if (resultAddEntries !== undefined) {
                Promise.all(resultAddEntries);
              }
            }

          });
          //     await Promise.all(resultEntriesWithInformation);

        }

        if (args.actorInfos.entries) {
          var existingEntries = [];
          result.entries.forEach(async (entry) => {
            if (parseInt(entry.id) > 0) {

              existingEntries.push(entry.id);

              if (!args.actorInfos.entries.includes('' + entry.id) ) {
                await result.removeEntry(entry);
              }
            }
          });

          args.actorInfos.entries.forEach(async (entry) => {
            if (parseInt(entry) > 0) {
              if (!existingEntries.includes(parseInt(entry)) && !entriesWithInformationAlreadeyCreated.includes(parseInt(entry))) {
                await result.addEntry(entry);
              }
            }
          });
        }

        if (args.actorInfos.referents) {
          var existingReferents = [];
          result.referents.forEach(async (referent) => {
            existingReferents.push(referent.id);

            if (!args.actorInfos.referents.includes(referent.id.toString())) {
              await result.removeReferent(referent);
            }
          });

          args.actorInfos.referents.forEach(async (referentId) => {
            if (!existingReferents.includes(parseInt(referentId))) {
              await result.addReferent(referentId);
              const referent = await User.findByPk(referentId);
              const emailTitle = "Vous êtes référent de " + result.name + " sur OUAAA!";

              const emailTemplate = 'addReferentActor';

              const data = {
                nomActeur: result.name,
                idActor: result.id,
                nomReferent: referent.surname,
                urlActorArea: process.env.CLIENT_URL + "/actorAdmin"
              };
              await sendEmail(referent.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);
            }
          });
        }
        if (args.actorInfos.memberOf) {
          var existingMemberOf = [];
          result.memberOf.forEach(async (memberOf) => {
            existingMemberOf.push(memberOf.id);

            if (!args.actorInfos.memberOf.includes(memberOf.id.toString())) {
              await result.removeMemberOf(memberOf);
            }
          });

          args.actorInfos.memberOf.forEach(async (memberOfId) => {
            if (!existingMemberOf.includes(parseInt(memberOfId))) {
              await result.addMemberOf(memberOfId);
            }
          });
        }


        if (args.logoPictures) {
          const pictures = await managePicture(args.logoPictures, result.id);
        }

        if (args.mainPictures) {
          const pictures = await managePicture(args.mainPictures, result.id);
        }
        if (args.pictures) {
          const pictures = await managePicture(args.pictures, result.id);
        }

        const actor = await Actor.findOne({
          where: { id: result.id },
          include: [
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },

                },

                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
              order: [{ model: ActorEntries }, 'topSEO', 'ASC'],
            },
            {
              model: Event,
              as: 'events',
              attributes: ['id', 'label'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'logo',
                    'main',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
              ],
            },
            {
              model: User,
              as: 'volunteers',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },

            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo',
                'main',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              separate: true,
              model: OpeningHours,
              as: 'openingHours',
              attributes: ['id', 'days', 'hours', 'place'],
            },
          ],
        });
        actor.openingHour = openingHourUpdated;
        return actor;
      } catch (err) {
        console.log(err);
      }
    },
    addActorVolunteer: async (parent, args, context, infos) => {
      try {
        const actor = await Actor.findOne({
          where: { id: args.actorId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname', 'email'],
              through: {
                attributes: ['user_id', 'actor_id'],
              },
            }]
        });
        await actor.addVolunteers(args.userId);

        var emailReferents = actor.email + ',';
        actor.referents.map((referent) => {
          emailReferents += referent.email + ',';
        });


        const emailTitle =
          "Nouveau bénévole";

        const emailTemplate = 'newVolunteer';
        const user = await User.findByPk(args.userId);

        const data = {
          actorName: actor.name,
          idActor: args.actorId,
          firstNameVolunteer: user.surname,
          lastNameVolunteer: user.lastname,
          emailVolunteer: user.email,
          urlActorArea: process.env.CLIENT_URL + "/actorAdmin",
        };

        await sendEmail(emailReferents + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);



        return true;
      } catch (error) {
        console.log(error);
      }
    },
    removeActorVolunteer: async (parent, args, context, infos) => {
      try {
        const actor = await Actor.findOne({ where: { id: args.actorId } });

        await actor.removeVolunteers(args.userId);
        return true;
      } catch (error) {
        console.log(error);
      }
    },
    validateGameActor: async (parent, args, context, info) => {
      try {
        const actor = await Actor.findOne({
          where: { id: args.actorId },
        });
        const user = await User.findByPk(args.userId);

        await actor.addGamers(args.userId);


        const emailTitle =
          "Bravo vous avez un nouvel acteur à votre actif pour le grand défi";

        const emailTemplate = 'validationGameActor';

        const data = {
          name: user.surname,
          actorname: actor.name
        };

        await sendEmail(user.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);

        return true;
      } catch (error) {
        console.log(error);
      }
    },
    validateActor: async (parent, args, context, info) => {
      try {
        const actorValiated = await Actor.findOne({
          where: { id: args.actorId },
        });
        await actorValiated.update({
          dateValidation: new Date(),
          isValidated: true,
        });

        await actorValiated.setUserValidated(args.userId);


        const emailTitle =
          "Vous êtes visible sur OUAAA!";

        const emailTemplate = 'validationActor';

        const data = {
          name: actorValiated.name,
          idActor: actorValiated.id
        };
        const user = await User.findByPk(args.userId);

        await sendEmail(actorValiated.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);

        return actorValiated;
      } catch (error) {
        console.log(error);
      }
    },
    deleteActor: async (parent, args, context, infos) => {
      try {
        const actor = await Actor.findOne({
          where: { id: args.actorId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id'],
              },
            },
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },

                },

                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
              order: [{ model: ActorEntries }, 'topSEO', 'ASC'],
            },
            {
              model: Event,
              as: 'events',
              attributes: ['id', 'label'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',

                    'position',
                    'logo',
                    'main',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
                {
                  model: User,
                  as: 'referents',
                  attributes: ['id', 'surname', 'lastname'],
                  through: {
                    attributes: ['user_id', 'event_id'],
                  },
                },
                {
                  model: Entry,
                  as: 'entries',
                  include: [
                    {
                      model: Entry,
                      as: 'parentEntry',
                      attributes: ['id', 'label', 'position'],
                      include: {
                        model: Collection,
                        as: 'collection',
                        attributes: ['id', 'code', 'label', 'position'],
                      },

                    },

                    {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  ],
                  order: [{ model: ActorEntries }, 'topSEO', 'ASC'],
                },
              ],
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
          ],
        });
        const referentsDeletion = actor.referents.map(async (referent) => {
          await actor.removeReferent(referent);
        });
        await Promise.all(referentsDeletion);
        const eventsDeletion = actor.events.map(async (event) => {
          if (args.deleteEvent && event.referents.length < 2) {

            const referentsDeletionEvent = event.referents.map(async (referent) => {
              await event.removeReferent(referent);
            });
            await Promise.all(referentsDeletionEvent);
            const entriesEventDeletion = event.entries.map(async (entry) => {
              await event.removeEntry(entry);
            });
            await Promise.all(entriesEventDeletion);

            const picturesEventDeletion = event.pictures.map(async (picture) => {
              await event.removePictures(picture);
            });
            await Promise.all(picturesEventDeletion);

            await event.removeActor(actor);

            await Event.destroy({
              where: {
                id: event.id,
              },
            });

          } else {
            await event.removeActor(actor);
          }

        });
        await Promise.all(eventsDeletion);
        const entriesActorDeletion = actor.entries.map(async (entry) => {
          await actor.removeEntry(entry);
        });
        await Promise.all(entriesActorDeletion);

        const picturesDeletion = actor.pictures.map(async (picture) => {
          await actor.removePictures(picture);
        });
        await Promise.all(picturesDeletion);
        // checker si le user est admin d'un actor qui est admin de l'actor
        const result = await Actor.destroy({
          where: {
            id: args.actorId,
          },
        });



        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
  Actor: {
    // resolver code for Actor fields
  }
};
const managePicture = async (pictures, actorId) => {
  //upload files
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true

            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToActor(
                image.file.filename,
                `${actorId}`,
              ),
            ])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${actorId}-${fileToken}-original`,
                  position: index,
                  actorId: actorId,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    const uploadedFiles = await uploadedFilesPromise;

    return uploadedFiles.map(async (file) => {
      if (!file.newpic && file.deleted) {
        // supprimer une image déja existante

        return new Promise((resolve, reject) => {
          Picture.findByPk(file.id).then((picture) => {
            fs.unlink(
              `/static/images/actor/${actorId}/${picture.originalPictureFilename}.jpeg`,
              () => {
                picture.destroy().then((value) => {
                  resolve({
                    id: file.id,
                    destroyed: value,
                  });
                });
              },
            );
          });
        });
      } else {
        // sinon traiter les images à updater

        if (file.newpic) {
          if (file.originalPictureFilename) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setActor(actorId);
            return result;
          } else {
            return null;
          }
        } else {
          // si l'image est deja existante, il faut updater sa position
          const result = Picture.update(
            file,
            // {
            // position : file.position
            // }
            { where: { id: file.id } },
          );
          // position = position +1;
          return result;
        }
      }
    });

    const result = await Promise.all(_);
    /*  result.map((picture) => {
        if(picture.dataValues){
        picture.dataValues.setActor(actorId)
        }
    });
*/

    //console.log(result);
  }
};
export default composeResolvers(actorResolvers, {
  'Mutation.createActor': [isAuthenticated()],
  'Mutation.editActor': [isAuthenticated()],
  'Mutation.deleteActor': [isAuthenticated()],
});
