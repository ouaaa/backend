import { User, CodeControls } from 'lib/sequelize/models';
import { comparePassword, hashPassword } from 'utils/passwordUtils';
import {
  AuthenticationError,
  ForbiddenError,
  UserInputError,
} from '@apollo/server';
import { GraphQLError } from 'graphql';
import sendResetPsswdEmail from 'lib/handlebars/sendResetPasswordEmail';
import validateEmailFormat from 'utils/validateEmailFormat';
import sendValidationEmail from 'utils/email';
import { issueEmailValidationToken } from 'services/auth.service';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { isAuthenticated } from '../middleware';

const authResolvers = {
  Query: {
    isLogged: async (req, args, context, info) => {
      if (context?.req?.session?.user?.id)
        return await User.findByPk(context.req.session.user.id);
    },
  },
  Mutation: {
    /*
     ** Login mutation
     ** if the login action succeeded : return the User type
     ** if the user is already logged : throw an ForbiddenError with a contextual message
     ** if the login action failed : throw an AuthenticationError with a contextual message
     */
    login: async (req, args, context, info) => {
      const user = await User.findOne({
        where: {
          email: args.email,
        },
      });

      /* no user associated to the email provided */
      if (!user)
        throw new GraphQLError( 'Erreur dans le login et/ou le mot de passe.');
      if (user && !user.isEmailValidated) {
        throw new GraphQLError(
          'Email non validé veuillez vérifier votre boite mail pour valider votre compte',
        );
      }

      const pwd = await hashPassword(args.password);
      const compare = await comparePassword(args.password, user.password);

      if (compare && user.isEmailValidated) {
        // test if the passwords match
        context.req.session.user = user;
         /* if (args.persistentConnection) {
           context.session.cookie.maxAge = 864000000; // 10 days
          } else {
           context.session.cookie.maxAge = null; // browser session maxAge
          }*/
        return user;
      }
      /* wrong password provided */
      throw new GraphQLError( 'Mauvais mot de passe.');
    },

    /*
     ** Logout mutation
     ** if the logout action succeded : return true and destroy the associated session on the server
     ** if the logout action failed : throw an ForbiddenError with a contextual message
     */
    logout: async (parent, args, context, info) => {
      context.req.session.user = null;
      return true;
    },

    /*
     ** Register mutation
     */
    register: async (parent, args, context, info) => {
      try {
        // create a token to validate the mail
        const token = issueEmailValidationToken();

        const user = await User.create({
          surname: args.surname,
          lastname: args.lastname,
          email: args.email,
          password: await hashPassword(args.password),
          role: 'user',
          isEmailValidated: false,
          validationEmailToken: token,
        });
        const compare = await comparePassword(args.password, user.password);
        if (user) {
          sendValidationEmail(user, token, (err, info) => {
            if (err) console.log(err);
          });
          return true;
        }
        return false;
      } catch (err) {
        console.log(err);
      }
    },

    /*
     ** Update password mutation
     */
     updateAccountPassword: async (parent, args, context, info) => {
      const currentUserId = context.req?.session?.user?.id;
      const user = await User.findByPk(currentUserId);

      if (!user) {
        throw new ForbiddenError('Utilisateur inconnu');
      }

      user.password = await hashPassword(args.password);

      await user.save();

      return true;
    },

    /* mutation to send an email for to reset the password */
    sendResetPasswordEmail: async (parent, args, context, info) => {
      const email = args.resetPasswordInfos.email;

      if (!validateEmailFormat(email)) {
        throw new GraphQLError("Le format de l'email n'est pas valide.");
      }


      const user = await User.findOne({ where: { email: email } });

      if (!user) {
        /* if there is no user associated to the email provided*/
        return -1;
      }

      const code = await CodeControls.createCodeInstance(
        'PASSWD_RENEWAL',
        user.email,
      );

      try {
        await sendResetPsswdEmail(email, user.surname, code.code);
      } catch (err) {
        console.error(err);
      }

      return code.id;
    },
    /* mutation to validate the reset password code */
    validateActionCode: async (parent, args, context, info) => {
      const codeControl = await CodeControls.findByPk(
        args.validateActionCodeInfos.codeId,
      );

      /* if there is node code assaciated to the provided id*/
      if (codeControl?.code === args.validateActionCodeInfos.code) {
        return true;
      }
      throw new GraphQLError('Code de validation incorrect');
    },

    updateForgotPassword: async (parent, args, context, info) => {
      try {
        if (!args.email || !args.codeId || !args.code || !args.password) {
          throw new Error('Bad information provided');
        }

        const codeControl = await CodeControls.findOne({
          where: {
            id: args.codeId,
            code: args.code,
            info1: args.email,
          },
        });

        if (!codeControl) {
          throw new GraphQLError('Code de validation incorrect');
        }

        const user = await User.findOne({
          where: {
            email: args.email,
          },
        });

        if (!user) {
          throw new GraphQLError('User not found');
        }

        await user.update({
          isEmailValidated: true,
          password: await hashPassword(args.password),
        });

        return true;
      } catch (error) {
        console.log(error);
        throw new GraphQLError('Erreur durant le changement de mot de passe');
      }
    },

    updateAccount: async (parent, args, context, info) => {
      const currentUserId = context.req?.session?.user?.id;
      const user = await User.findByPk(currentUserId);

      if (!user) {
        throw new ForbiddenError('Utilisateur inconnu');
      }

      user.surname = args.surname;
      user.lastname = args.lastname;
      user.email = args.email;

      await user.save();
      return user;
    },

    deleteAccount: async (parent, args, context, infos) => {
      try {
        const currentUserId = context.req?.session?.user?.id;
        const result = await User.destroy({
          where: {
            id: currentUserId,
          },
        });
        context.req.session.user = null;

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
};

export default composeResolvers(authResolvers, {
  'Query.isLogged': [],
  'Query.isAccountValidated': [],
  'Mutation.login': [],
  'Mutation.logout': [isAuthenticated()],
  'Mutation.validateAccount': [],
  'Mutation.updateAccount': [isAuthenticated()],
  'Mutation.updateAccountPassword': [isAuthenticated()],
  'Mutation.deleteAccount': [isAuthenticated()],
  'Mutation.sendResetPasswordEmail': [],
  'Mutation.validateActionCode': [],
  'Mutation.updateForgotPassword': [],
});
