import { Actor, Event, Article } from 'lib/sequelize/models';
const { Op } = require('sequelize');
const Sequelize = require('sequelize');

const searchResolvers = {
  Query: {
    search: async (parent, args, context, info) => {
      var actors;
      var events;
      var articles;
      const result = {}
      try {
      if(args.searchActor){
        actors = await Actor.findAll({
          where: {
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('activity')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
            ],
            isValidated : true
          }
        });
        result.actors=actors;
      }
      if(args.searchEvent){
        events = await Event.findAll({
          where: {
            [Op.and]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('label')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.col('endedAt'), {
                [Op.gte]: new Date(),
                }),
            ],
          }
        });
        result.events=events;
      }
      if(args.searchArticle){ 
        articles = await Article.findAll({
          where: {
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('label')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
                [Op.like]: `%${args.searchValue.toLowerCase()}%`,
                }),
              ],
          }
        });
        result.articles=articles;
      }
      
       
        return result;
      } catch (error) {
        console.log(error);
      }
    }
  },
  Mutation: {

  },
};

export default searchResolvers;
