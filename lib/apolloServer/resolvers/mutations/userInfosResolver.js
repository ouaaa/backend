import { ForbiddenError } from '@apollo/server';
import { User, Event, Actor } from 'lib/sequelize/models';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { isAdmin, isAuthenticated } from '../middleware';
const userResolvers = {
  Query: {
    users: async (parent, args, context, info) => {
      const users = await User.findAll({
        include: [
          {
            model: Actor,
            as: 'adminactors',
          },
        ]
      });
      const formattedUsers = users.map(u => {
        const user = u.get({ plain: true});
        return { ...user, actors: user.adminactors };
      });
      return formattedUsers;
    },
  user: async (parent, args, context, info) => {
    const user = await User.findOne({
      where: { id: args.id },
      include: [
        {
          model: Actor,
          as: 'adminactors',
        },
      ]
    });

    const u = user.get({ plain: true});
    return { ...u, actors: u.adminactors };
  },
},
  Mutation: {
    /* mutation to update user's infos */
    updateUserInfos: async (parent, args, context, info) => {
      const user = await User.findByPk(args.userId);

      if (args.userInfos.id != user.id)
        throw new ForbiddenError('Vous ne pouvez pas effectuer cette action.');

      user.surname = args.userInfos.surname;
      user.lastname = args.userInfos.lastname;
      user.email = args.userInfos.email;
      user.phone = args.userInfos.phone;
      user.address = args.userInfos.address;
      user.postCode = args.userInfos.postCode;
      user.city = args.userInfos.city;

      await user.save();
      return user;
    },
    deleteUser: async (parent, args, context, infos) => {
      try {
        const user = await User.findOne({
          where: { id: args.userId },
          include: [
            {
              model: Event,
              as: 'adminevents',
              attributes: ['id'],
              through: {
                attributes: ['user_id', 'event_id'],
              }
            },{
              model: Actor,
              as: 'adminactors',
              attributes: ['id'],
              through: {
                attributes: ['user_id', 'actor_id'],
              }
            }
          ],
        });
        const referentsDeletion = user.events.map(async (event) => {
          await event.removeReferent(user);
        });
        await Promise.all(referentsDeletion);
        

        
        const result = await User.destroy({
          where: {
            id: args.userId,
          },
        });

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
};


export default composeResolvers(userResolvers, {
  'Query.users': [isAuthenticated()],
  'Query.user': [isAuthenticated(), isAdmin()],
  'Mutation.addUser': [isAuthenticated(), isAdmin()],
  'Mutation.sendValidationEmail': [isAuthenticated()],
  'Mutation.editUser': [isAuthenticated(), isAdmin()],
  'Mutation.updateUserInfos': [isAuthenticated()],
  'Mutation.deleteUser': [isAuthenticated(), isAdmin()],
});