import { toUpperCase } from "lib/handlebars/helpers";
import { sendEmail } from "lib/nodemailer/sendEmail";
import {
  Actor, User
} from 'lib/sequelize/models';
const sendContactFormEmailResolver = {
  Mutation: {
    sendContactFormEmail: async (parentValue, args, context) => {
      const helpers = {
        uppercase: toUpperCase
      };
      const attachments = [];

      try {
        if (args.contactForm?.pictures) {
          let i = 0;
          for (const picture of args.contactForm.pictures) {
            const { createReadStream, filename, mimetype, encoding } = await picture;
            const stream = createReadStream();

            attachments.push({
              filename: `attachment-${i}.jpeg`,
              content: stream
            });
            i++;
          }
        }
      } catch (error) {
        console.error(error);
      }

      sendEmail(
        process.env.EMAIL_CONTACT,
        `[${args.contactForm.category?.toUpperCase()}] Nouveau message depuis la plateforme`,
        'contactForm',
        args.contactForm,
        helpers,
        attachments
      );
      return true;
    },
    inviteActor: async (parentValue, args, context) => {
      const helpers = {
        uppercase: toUpperCase
      };
      args.inviteActorInfos.hasMessage=args.inviteActorInfos.message?true:false;
      args.inviteActorInfos.requesterName=args.inviteActorInfos.firstName?(args.inviteActorInfos.firstName+ ' '+args.inviteActorInfos.lastName):args.inviteActorInfos.requesterName;
      args.inviteActorInfos.hasRequester=args.inviteActorInfos.requesterName?true:false;
      if(args.inviteActorInfos.sendEmail){
        sendEmail(
          args.inviteActorInfos.actorEmail,
          (args.inviteActorInfos.contactName?`${args.inviteActorInfos.contactName}, `:``)+(args.inviteActorInfos.requesterName?`${args.inviteActorInfos.requesterName}`:`une personne`)+` vous sollicite pour inscrire ${args.inviteActorInfos.actorName} sur OUAAA!`,
          'inviteActor',
          args.inviteActorInfos,
          helpers,
        );
      }
    /*  const inviteActor = await InviteActor.create({
        requesterName: args.inviteActorInfos.requesterName,
        actorName: args.inviteActorInfos.contactName,
        actorEmail: args.inviteActorInfos.actorEmail,
        postCode: args.inviteActorInfos.postCode,
        message: args.inviteActorInfos.message,
        sendEmail : args.inviteActorInfos.sendEmail,
      });
       */
      return true;
    },
    suggestEvent: async (parentValue, args, context) => {
      const helpers = {
        uppercase: toUpperCase
      };
   
      const actor = await Actor.findOne({ where: { id: args.suggestEventInfos.actorId } ,
        include: [
          {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname','email'],
            through: {
              attributes: ['user_id', 'actor_id'],
            },
          }]});
    
      var emailReferents = actor.email+',' ; 
      actor.referents.map((referent) => {
        emailReferents += referent.email+',';
      });
      args.suggestEventInfos.hasMessage=args.suggestEventInfos.message?true:false;
      args.suggestEventInfos.requesterName=args.suggestEventInfos.firstName?(args.suggestEventInfos.firstName+ ' '+args.suggestEventInfos.lastName):args.suggestEventInfos.requesterName;
      args.suggestEventInfos.hasRequester=args.suggestEventInfos.requesterName?true:false;
      args.suggestEventInfos.actorName=actor.name;
        sendEmail(
          emailReferents,
          (args.suggestEventInfos.contactName?`${args.suggestEventInfos.contactName}, `:``)+(args.suggestEventInfos.requesterName?`${args.suggestEventInfos.requesterName}`:`une personne`)+` vous propose d'ajouter votre événement sur OUAAA!`,
          'suggestEvent',
          args.suggestEventInfos,
          helpers,
        );
      return true;
    }
   
  },
}




export default sendContactFormEmailResolver;
