import { Op } from 'sequelize';
import {
  Resource,
  Recipe,
  User,
  Picture,
  Ingredient,
  IngredientBaseAlim,
  Entry,
  Actor
} from 'lib/sequelize/models';
import randToken from 'rand-token';
import fs from 'fs';
import {transformNameToUrl} from 'utils/urlUtils';
const recipeResolvers = {
  Query: {
    recipes: async (parent, args, context, info) => {
      let whereCondition = {};

      if (args.search) {
        whereCondition = {
          where: {
            [Op.or]: {
              label: {
                [Op.like]: `%${args.search}%`
              },
              shortDescription: {
                [Op.like]: `%${args.search}%`
              },
            }
          }
        }
      }

      const recipes = await Recipe.findAll({
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          },
          {
            model: Ingredient,
            as: 'ingredients'
          }
        ],
        ...whereCondition,
      });

      return (recipes || []);
    },
    recipe: async (parent, args, context, info) => {
      var whereCondition = {};
      if (args.id != undefined) {
        whereCondition['id'] = args.id;
      } else if (args.url != undefined) {
        if (args.url != undefined) {
          whereCondition['url'] = args.url;
        }
      }
      const recipe = await Recipe.findOne({
        where: whereCondition,
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          },
          {
            model: User,
            as : 'user',
          },
          {
            model: Actor,
            as : 'actor',
          },
          {
            model: Ingredient,
            as: 'ingredients',
            include: [
              {
                model: IngredientBaseAlim,
              }
            ]
          }]
      });
      return (recipe || []);
    },
    recipesAdmin: async (parent, args, context, info) => {
      const { userId } = args;
    
      // Récupérer l'utilisateur
      const user = await User.findByPk(userId);
    
      if (!user) {
        throw new Error('User not found');
      }
    
      // Construire la condition WHERE en fonction du rôle de l'utilisateur
      let whereCondition = {};
    
      if (user.role === 'user') {
        whereCondition = {
          [Op.or]: [
            { user_id: userId }, // Assurez-vous que 'user_id' correspond au nom de la colonne dans la table `Recipe`
          ],
        };
      }
    
      try {
        // Récupérer les recettes avec les inclusions nécessaires
        const recipes = await Recipe.findAll({
          where: whereCondition,
          include: [
            {
              model: Picture,
              as: 'pictures', // Alias défini dans les relations
              separate: true, // Chargement séparé si activé
            },
            {
              model: Ingredient,
              as: 'ingredients', // Alias défini dans les relations
            },
            {
              model: User,
              as: 'user', // Alias défini dans les relations
              attributes: ['id', 'surname', 'lastname'], // Sélection des colonnes
            },
          ],
        });
    
        // Retourner les recettes trouvées ou un tableau vide
        return recipes || [];
      } catch (error) {
        console.error('Error fetching recipes:', error.message);
        throw new Error('Unable to fetch recipes');
      }
    },

    ingredientBaseAlim: async (parent, args, context, info) => {
      const ingredientBaseAlim = await IngredientBaseAlim.findAll({
      });
      return (ingredientBaseAlim || []);
    }
  },
  Mutation: {
    createRecipe: async (parent, args, context, infos) => {

      let recipe = await Recipe.build({
        ...args.recipe,
        url : transformNameToUrl(args.recipe.label),
      });

      recipe=  await recipe.save();

      if (args.actorId) {
        await recipe.setActor(args.actorId);
      }

      if (args.userId) {
        await recipe.setUser(args.userId);
      }

      recipe=  await recipe.save();

   
    
  
      if (args.recipe.ingredients) {
        args.recipe.ingredients.forEach(async (ingredientData) => {
          let ingredient = await Ingredient.build({
            ...ingredientData,
            recipe_id: recipe.id,
          });
          ingredient = await ingredient.save();
        
        });
    }
      if (args.mainPictures) {
        await uploadImage(
          args.mainPictures,
          recipe.id,
        );
      }
      if (args.pictures) {
        await uploadImage(
          args.pictures,
          recipe.id,
        );
      }

      await Resource.create({
        type: 'RECIPE',
        recipe_id: recipe.id,
      });

      return recipe;
    },
    editRecipe: async (parent, args, context, infos) => {  

      const updateUrl = await Entry.findOne({ where: { code: 'updateUrlRecipe' } });
      if(updateUrl){
        const recipes = await Recipe.findAll();
        recipes.forEach(async (event) => {
          await recipe.update({
            url:  transformNameToUrl(recipe.label)
          });
      })
      updateUrl.destroy();
      }
      let recipe = await Recipe.findOne({
        where: { id: args.recipeId },
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          },
          {
            model: Ingredient,
            as: 'ingredients'
          }
        ]
      });

      if (args.actorId) {
        await recipe.setActor(args.actorId);
      }

      if (args.recipe) {
        await recipe.update({
          ...args.recipe,
          url : transformNameToUrl(args.recipe.label),
        });
      }
      if (args.mainPictures) {
        await uploadImage(
          args.mainPictures,
          recipe.id,
        );
      }
      if (args.pictures) {
        await uploadImage(
          args.pictures,
          recipe.id,
        );
      }

      if (args.recipe.ingredients) {
        await Ingredient.destroy({
          where: {
            recipe_id: recipe.id,
          }
        });

        args.recipe.ingredients.forEach(async (ingredientData) => {
          let ingredient = await Ingredient.build({
            ...ingredientData,
            recipe_id: recipe.id,
          });
          ingredient = await ingredient.save();
        });
      }
      recipe = await Recipe.findOne({
        where: { id: args.recipeId },
        include: [
          {
            model: Picture,
            as: 'pictures',
            separate: true,
          },
          {
            model: Ingredient,
            as: 'ingredients',
            include: [
              {
                model: IngredientBaseAlim,
              }
            ]
          }]
      });
      return recipe;
    },
  },
  Recipe: {
    /* collecton's author */
  },
};

const moveNewPictureToRecipe = async ( filename, id) => {
  let dirpath = `./public/static/images/recipe/${id ? id : ''}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/recipe/${id}/${filename}`;
 
  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/`+filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path );
  })
  return {
    url,
    filename,
    path,
  };


};


const uploadImage = async (pictures, recipeID) => {
  //upload files
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true

            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToRecipe(
                image.file.filename,
                `${recipeID}`
              )])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${recipeID ? (recipeID) : ''}-${fileToken}-original`,
                  position: index,
                  recipeID: recipeID,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    if (recipeID) {
      const uploadedFiles = await uploadedFilesPromise;

      return uploadedFiles.map(async (file) => {
        if (!file.newpic && file.deleted) {
          // supprimer une image déja existante

          return new Promise((resolve, reject) => {
            Picture.findByPk(file.id).then((picture) => {
              fs.unlink(
                `/static/images/recipe/${recipeID}/${picture.originalPictureFilename}.jpeg`,
                () => {
                  picture.destroy().then((value) => {
                    resolve({
                      id: file.id,
                      destroyed: value,
                    });
                  });
                },
              );
            });
          });
        } else {
          // sinon traiter les images à updater

          if (file.newpic) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setRecipe(recipeID);
            return result;
          } else {
            // si l'image est deja existante, il faut updater sa position
            const result = Picture.update(
              file,
              // {
              // position : file.position
              // }
              { where: { id: file.id } },
            );
            // position = position +1;
            return result;
          }
        }
      });
      const result = await Promise.all(_);
    }


    /*  result.map((picture) => {
        if(picture.dataValues){
        picture.dataValues.setActor(actorId)
        }
    });
*/

    //console.log(result);
  }
};
export default recipeResolvers;