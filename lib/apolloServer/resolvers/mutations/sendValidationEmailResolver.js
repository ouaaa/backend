import { User } from 'lib/sequelize/models';
import validateEmailFormat from 'utils/validateEmailFormat';
import sendValidationEmailFunc from 'utils/email';
import {
  issueEmailValidationToken,
  verifyAuthentification,
} from 'services/auth.service';
import { SEND_EMAIL_VALIDATION_AUTH } from 'config/constants/constants';

const sendValidationEmailResolver = {
  Mutation: {
    sendValidationEmail: async (parentValue, args, context) => {
      if (verifyAuthentification(context, SEND_EMAIL_VALIDATION_AUTH)) {
        if (!args.email) throw new Error('Bad email information');

        if (!validateEmailFormat(args.email))
          throw new Error('Email format is wrong');

        let user = null;

        try {
          user = await User.findOne({
            where: {
              email: args.email,
            },
          });
        } catch (err) {
          console.log(err);
          throw new Error('Global mutation error');
        }

        if (!user)
          throw new Error('Email not known, please create a new account');
        else {
          if (user.isEmailValidated)
            throw new Error('User email address is already validated');
          else {
            const validationEmailToken = issueEmailValidationToken();
            await user.update({ validationEmailToken: validationEmailToken });
            sendValidationEmailFunc(user, user.validationEmailToken);
            return true;
          }
        }
      }
    },
  },
};

export default sendValidationEmailResolver;
