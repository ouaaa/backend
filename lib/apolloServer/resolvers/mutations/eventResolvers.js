import {
  Actor,
  Event,
  Newsletter,
  User,
  Picture,
  Entry,
  Collection
} from 'lib/sequelize/models';
import { DataTypes, Op } from 'sequelize';
import { sendEmail } from 'lib/nodemailer/sendEmail';
import randToken from 'rand-token';
import fs from 'fs';
import { RRule } from 'rrule';
import moment from 'moment';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { GraphQLError } from 'graphql';
import { isAuthenticated } from '../middleware';
import {transformNameToUrl} from 'utils/urlUtils';

const Sequelize = require('sequelize');

function transformToUrl (name, date){
  return transformNameToUrl(name)+ '-' + moment(date).format('DD-MM-YYYY');
}
const moveNewPictureToEvent= async ( filename, id) => {
  let dirpath = `./public/static/images/event/${id ? id : ''}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/event/${id}/${filename}`;
  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/`+filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path );
  })

  return {
    url,
    filename,
    path,
  };


};

const eventResolvers = {
  Query: {
    events: async (parent, args, context, info) => {
      let events;
      var allEntries = [];
      if (args.entries != undefined && args.entries.length != 0) {
        args.entries.forEach((entries) => {
          if (entries != undefined) {
            entries.forEach((entry) => {
              allEntries.push(entry);
            });
          }
        });
      }
      var whereCondition = {};
      var includeCondition = {};
      var includeActor = {};
      let params = {};
       params.attributes = {
        include: [
          [
            Sequelize.literal(`(SELECT COUNT(*) FROM user_event_participant uep WHERE uep.event_id = Event.id)`),
            'nbParticipants'
          ]
        ]
      };
      if (args.startingDate) {
        args.startingDate = moment(args.startingDate).startOf('day').toDate();
      }
      //TODO do a and with the search
      if (args.search != undefined && args.search.length != 0) {
        whereCondition['label'] = Sequelize.where(Sequelize.fn('lower', Sequelize.col('`Event`.label')), {
          [Op.like]: `%${args.search.toLowerCase()}%`,
        });
      } else {
        if (args.startingDate) {
          whereCondition['endedAt'] = Sequelize.where(Sequelize.col('`Event`.endedAt'), {
            [Op.gte]: args.startingDate,
          });
        }
        if (args.notFinished) {
          whereCondition['endedAt'] = Sequelize.where(Sequelize.col('`Event`.endedAt'), {
            [Op.gte]: new Date(),
          });
        }
      }

      if (args.favoritesForUser != undefined && args.favoritesForUser != null) {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          where: {
            id: args.favoritesForUser,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          through: {
            attributes: [],
          },
        };
      }

      if (args.actorId) {
        includeActor = {
          model: Actor,
          as: 'actors',
          attributes: ['id','url'],
          where: {
            id: args.actorId,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeActor = {
          model: Actor,
          as: 'actors',
          attributes: ['id', 'name','url'],
          through: {
            attributes: [],
          },
        };
      }
      //TODO refacto avec condition pour éviter la duplication (sutout si on rajoute le lieu dans le filtre)
      if (allEntries.length != 0) {
        if (args.startingDate) {
          let eventsBeforeFiltering;

          eventsBeforeFiltering = await Event.findAll({
            where: whereCondition,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                where: {
                  id: {
                    $or: allEntries,
                  },
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Actor,
                as: 'actors',
                attributes: ['id', 'name','url'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label','url'],
              },
            ],
          });
          var eventsFilterd = [];

          eventsBeforeFiltering.forEach(async (event) => {
            let isOk = true;
            args.entries.forEach((entries) => {
              if (entries != undefined) {
                let isContained = false;
                event.entries.forEach((eventEntry) => {
                  if (entries.includes('' + eventEntry.id)) {
                    isContained = true;
                  }
                });

                if (entries.length != 0 && !isContained) {
                  isOk = false;
                }
              }
            });
            if (isOk) {
              eventsFilterd.push(event);
            }
          });

          events = eventsFilterd;
        } else {
          events = await Event.findAll({
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Actor,
                as: 'actors',
                attributes: ['id', 'name','url'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label','url'],
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        }
      } else {
        if (args.startingDate) {
          events = await Event.findAll({
            where: whereCondition,
            order: [
              [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
            ],
            limit: args.limit ? args.limit : 1000,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Actor,
                as: 'actors',
                attributes: ['id', 'name','url'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label'],
              },

              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        } else if (args.notFinished) {
          events = await Event.findAll({
            where: whereCondition,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Actor,
                as: 'actors',
                attributes: ['id', 'name','url'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        } else {
          events = await Event.findAll({
            where: whereCondition,
            order: [
              [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
            ],
            limit: args.limit,
            attributes: {
              include: [
                [
                  Sequelize.literal(`(SELECT COUNT(*) FROM user_event_participant uep WHERE uep.event_id = Event.id)`),
                  'nbParticipants'
                ]
              ]
            },
            include: [
              includeCondition,
              includeActor,

              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              }
            ],
          });
        }
      }
      whereCondition = {};
      whereCondition['dateRule'] = {
        [Op.not]: null,
      };
      if (args.search != undefined && args.search.length != 0) {
        whereCondition['label'] = Sequelize.where(Sequelize.fn('lower', Sequelize.col('`Event`.label')), {
          [Op.like]: `%${args.search.toLowerCase()}%`,
        });
      }

      if (args.periodicEvent===undefined || args.periodicEvent===true) {
        let eventsWithRules = await Event.findAll({

          where: whereCondition,
          order: [
            [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
          ],
          limit: args.limit,
          include: [
            includeCondition,
            includeActor,
            {
              model: Entry,
              as: 'entries',
              attributes: ['id', 'label', 'code', 'color', 'icon'],
              through: {
                attributes: ['entry_id', 'event_id'],
              },
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },
                }
              ]
            },
            {
              model: Actor,
              as: 'actors',
              attributes: ['id', 'name','url'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo',
                'main',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              model: Event,
              as: 'parentEvent',
              attributes: ['id', 'label','url'],
            },
            {
              model: User,
              as: 'favorites',
              attributes: ['id'],
              through: {
                attributes: [],
              },
            },
          ],
        });


        eventsWithRules.forEach((eventWithRules) => {

          const startDate = moment(parseInt(eventWithRules.startedAt.getTime()));
          const dateRule = eventWithRules.dateRule;


          const rrule = RRule.fromString('DTSTART:' + startDate.format('YYYYMMDD[T]hhmmss[Z]') + '\nRRULE:' + dateRule);
          let startDateSearch;
          if (args.startingDate) {
            startDateSearch = Date.parse(args.startingDate);
          }
          if (args.notFinished) {
            startDateSearch = new Date();
          }
          let endDate;
          if (startDateSearch) {
            endDate = rrule.after(new Date(startDateSearch));
          }



          if ((!endDate || endDate > startDateSearch) && !events.some(e => e.id === eventWithRules.id)) {
            events.push(eventWithRules);
          }

        });
    }

      if (args.canAdmin) {
        if (context.req?.session?.user?.id && context.req?.session?.user?.role === 'admin') {
          return events;
        } else if (context.req?.session?.user?.id) {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Actor,
                as: 'adminactors',
              },
            ]
          });

          const u = user.get({ plain: true});
          const actors = u.adminactors.map(s => s.id);

          return events
            .map(event => event.get({ plain: true}))
            .filter(event => event.actors
              .map(s => s.id)
              .some(s => actors.includes(s))
            );

        } else {
          return [];
        }
      }

      return events;
    },
    event: async (parent, args, context, info) => {
      var whereCondition = {};
      if (args.id != undefined) {
        whereCondition['id'] = args.id;
      } else if (args.url != undefined) {
        if (args.url != undefined) {
          whereCondition['url'] = args.url;
        }
      }

      const event = await Event.findOne({
        where: whereCondition,
        include: [
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
          {
            model: Actor,
            as: 'actors',
            attributes: ['id', 'name','url'],
            through: {
              attributes: [],
            },

            include: [
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'main',
                  'logo',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname'],
                through: {
                  attributes: [],
                },
              },
            ],
          },
          {
            model: User,
            as: 'participants',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          }, {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'main',
              'logo',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            model: Entry,
            as: 'entries',
            include: [
              {
                model: Entry,

                as: 'parentEntry',
                attributes: ['id', 'label', 'position'],
                include: {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },

              },
              {
                model: Collection,

                as: 'collection',
                attributes: ['id', 'code', 'label', 'position'],
              },
            ]
          },
          {
            model: Event,
            as: 'parentEvent',
            attributes: ['id', 'label', 'startedAt', 'endedAt','url'],
            include: [
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'main',
                  'logo',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
            ]
          },
          {
            model: Event,
            as: 'subEvents',
            separate: true,
            attributes: ['id', 'label', 'startedAt', 'endedAt', 'city', 'address'],
          },

        ],
      });

      return event;
    },
    eventsAdmin: async (parent, args, context, info) => {
      let events;
      if (context?.req?.session?.user) {
        let params = {
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id', 'event_id'],
              },
            },
          ],
        };

        params.attributes = {
          include: [
            [
              Sequelize.literal(`(SELECT COUNT(*) FROM user_event_participant uep WHERE uep.event_id = Event.id)`),
              'nbParticipants'
            ]
          ]
        };

        if (context?.req?.session?.user.role !== 'admin') {
          params.include[0].where = { id: args.userId };
        }

        events = await Event.findAll(params, {
          orderBy: [
            // ['activated','DESC'],
            ['startedAt', 'DESC'],
          ]
        });
        //Find actor referent
        if (context?.req?.session?.user.role !== 'admin') {
          let actors;
          let paramsActor = {
            include: [
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname'],
                through: {
                  attributes: ['user_id', 'actor_id'],
                },
              },
            ],
          };

          paramsActor.include[0].where = { id: args.userId };


          actors = await Actor.findAll(paramsActor);

          let actorIdArray = [];
          actors.forEach((actor) => {
            actorIdArray.push(actor.id);
          });



          params.include[0] = {
            model: Actor,
            as: 'actors',
            attributes: ['id', 'name','url'],
            through: {
              attributes: [],
            }
          };
          params.include[1] = {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: ['user_id', 'event_id'],
            },
          }
          params.include[0].where = { id: actorIdArray };
        }
        events = Array.from(new Set(events.concat(await Event.findAll(params, {
          orderBy: [
            ['startedAt', 'ASC'],
          ]
        }))));
        events = [...new Map(events.map(event =>
          [event['id'], event])).values()];
      }
      events = events.sort((a, b) => (a.startedAt < b.startedAt ? 1 : -1));
      return events;
    },
    participants: async (parent, args, context, info) => {
      let participants = [];

      if (context?.req?.session?.user) {
        participants = User.findAll({
          include: [
            {
              model: Event,
              as: 'events',
              where: {
                id: args.eventId
              }
            },
          ],
          attributes: {
            include: [
              [
                Sequelize.literal(
                  `(SELECT createdAt FROM user_event_participant uep WHERE uep.event_id = ${args.eventId} AND uep.user_id = User.id)`
                ),
                'participatedAt'
              ]
            ]
          }
        });
      }

      return participants;
    }
  },
  Mutation: {
    createEvent: async (parent, args, context, infos) => {
      try {
    
        const event = await Event.create({
          label: args.eventInfos.label,
          shortDescription: args.eventInfos.shortDescription,
          facebookUrl: args.eventInfos.facebookUrl,
          description: args.description,
          startedAt: args.eventInfos.startedAt,
          endedAt: args.eventInfos.endedAt,
          dateRule: args.eventInfos.dateRule,
          published: args.eventInfos.published,
          lat: args.eventInfos.lat,
          lng: args.eventInfos.lng,
          address: args.eventInfos.address,
          postCode: args.eventInfos.postCode,
          city: args.eventInfos.city,
          practicalInfo: args.practicalInfo,
          registerLink: args.eventInfos.registerLink,
          limitPlace: args.eventInfos.limitPlace,
          event_id: args.eventInfos.parentEventId,
          participateButton : args.eventInfos.participateButton,
          url: transformToUrl(args.eventInfos.label,args.eventInfos.startedAt),
        });
        await event.addActor(args.actorId);
        if (args.eventInfos.actors) {
          args.eventInfos.actors.forEach(async (actorId) => {
            await event.addActor(actorId);
          });
        }

        await event.addReferent(args.userId);

        if (args.logoPictures) {
          const pictures = await uploadImage(
            args.logoPictures,
            event.id,
          );
        }

        if (args.mainPictures) {
          await uploadImage(
            args.mainPictures,
            event.id,
          );
        }
        if (args.pictures) {
          await uploadImage(args.pictures, event.id);
        }

        if (args.eventInfos.entries) {
          args.eventInfos.entries.forEach(async (entry) => {
            if (parseInt(entry) > 0) {
                await event.addEntry(entry);
            }
          });
        }
        const actor = await Actor.findOne({
          where: { id: args.actorId }
        });
        
        let referentEmails = '';
        if (actor) {
            const referents = await actor.getReferents();
            referentEmails = referents.map(referent => referent.email).join(',');

      
          if(args.eventInfos.referencingActor){
            const emailTemplate = 'eventCreationEmailReferencingActor';
            
            const referenceur = await Actor.findOne({
              where: { id: args.eventInfos.referencingActor }
            });
      
            const emailTitle = referenceur.name+" a ajouté un de vos événements sur OUAAA!";

          
            const data = {
              actorName:  actor.name,
              referencingActorName: referenceur.name,
              eventName: event.label,

              url: process.env.CLIENT_URL + "/actorAdmin/event/" + event.id ,
            };
            const user = await User.findByPk(args.userId);
            await sendEmail(referentEmails+ ',' + user.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);
    

          }

          if(args.eventInfos.inviteActors){
            const emailTemplate = 'eventEmailInviteActor';
          
            args.eventInfos.inviteActors.forEach(async (actorId) => {
            const inviteActor = await Actor.findOne({
              where: { id: actorId }
            });
            
            const emailTitle = inviteActor.name +" , "+actor.name +" vous propose de participer à son événéement " + event.label ;
            let referentsInvited = '';
            if (inviteActor) {
              referentsInvited = await inviteActor.getReferents();
              referentsInvited = referentsInvited.map(referent => referent.email).join(',');
            }

            const data = {
              actorInvitedName:  inviteActor.name,
              actorName:  actor.name,
              eventName: event.label,
              invitationMessage: args.eventInfos.emailInvitationText,
              date: moment(event.startedAt).format('DD/MM/YYYY HH:mm'),
              adress: event.address + ' ' +  + event.city,
              url: process.env.CLIENT_URL + "/evenement/" + event.url ,
            };
            await sendEmail(actor.email + ',' +referentsInvited + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data, null, null, null,referentEmails);

          });
        }
      }
        return event;
      } catch (err) {
        console.log(err);
      }
    },
    editEvent: async (parent, args, context, infos) => {

      //TODO remove after all events are updated
      const updateUrl = await Entry.findOne({ where: { code: 'updateUrlEvent' } });
      if(updateUrl){
        const events = await Event.findAll();
        events.forEach(async (event) => {
          await event.update({
            url: transformToUrl(event.label,event.startedAt)
          });
      })
      updateUrl.destroy();
      }
      
      try {
        const getEvent = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Actor,
              as: 'actors',
              attributes: ['id', 'name','url'],
              through: {
                attributes: [],
              },
            },
          ],
        });

        // check si le user est admin d'une étape qui est admin de l'event
        if (context.req?.session?.user?.role !== 'admin') {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Actor,
                as: 'adminactors',
              },
            ]
          });

          const u = user.get({ plain: true});
          const actors = u.adminactors.map(s => s.id);

          if (getEvent.get({ plain: true}).actors.map(s => s.id).every(s => !actors.includes(s))) {
            throw new GraphQLError('Not allowed to edit this event');
          }
        }

        const event = await Event.update(
          {
            label: args.eventInfos.label,
            shortDescription: args.eventInfos.shortDescription,
            facebookUrl: args.eventInfos.facebookUrl,
            description: args.description,
            startedAt: args.eventInfos.startedAt,
            endedAt: args.eventInfos.endedAt,
            published: args.eventInfos.published,
            lat: args.eventInfos.lat,
            lng: args.eventInfos.lng,
            address: args.eventInfos.address,
            postCode: args.eventInfos.postCode,
            city: args.eventInfos.city,
            practicalInfo: args.practicalInfo,
            registerLink: args.eventInfos.registerLink,
            limitPlace: args.eventInfos.limitPlace,
            event_id: args.eventInfos.parentEventId,
            dateRule: args.eventInfos.dateRule,
            participateButton : args.eventInfos.participateButton,
            url: transformToUrl(args.eventInfos.label,args.eventInfos.startedAt),
          },
          {
            where: {
              id: args.eventId,
            },
          },
        );


        const result = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Entry,
              as: 'entries',
              attributes: ['id'],
            },
            {
              model: Actor,
              as: 'actors',
              attributes: ['id', 'name','url'],
              through: {
                attributes: [],
              },
            },

          ],
        });

        if (args.logoPictures) {
          await uploadImage(args.logoPictures, result.id);
        }

        if (args.mainPictures) {
          await uploadImage(args.mainPictures, result.id);
        }
        if (args.pictures) {
          await uploadImage(args.pictures, result.id);
        }

        if (args.eventInfos.actors) {
          var existingActors = [];
          result.actors.forEach(async (actor) => {
            existingActors.push(actor.id);

            if (!args.eventInfos.actors.includes(actor.id.toString())) {
              await result.removeActor(actor);
            }
          });

          args.eventInfos.actors.forEach(async (actorId) => {
            if (!existingActors.includes(parseInt(actorId))) {
              await result.addActor(actorId);
            }
          });
        }

        const eventWithCategory = await Event.findOne({
          where: { id: args.eventId },
        });
        if (args.eventInfos.entries) {
          var existingEntries = [];
          result.entries.forEach(async (entry) => {
            existingEntries.push(entry.id);

            if (!args.eventInfos.entries.includes('' + entry.id)) {
              await result.removeEntry(entry);
            }
          });

          args.eventInfos.entries.forEach(async (entry) => {
            if (!existingEntries.includes(parseInt(entry))) {
              await result.addEntry(entry);
            }
          });
        }

        //TODO mettre en commum avec la methode event
        const eventReloaded = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Actor,
              as: 'actors',
              attributes: ['id', 'name','url'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'main',
                    'logo',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
                {
                  model: User,
                  as: 'referents',
                  attributes: ['id', 'surname', 'lastname'],
                  through: {
                    attributes: [],
                  },
                },
              ],
            },
            {
              model: User,
              as: 'participants',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            }, {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'main',
                'logo',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },
                },
                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
            },
          ],
        });

        return eventReloaded;
      } catch (err) {
        console.log(err);
      }
    },
    addEventParticipate: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [{

            model: Actor,
            as: 'actors',
            attributes: ['id', 'name','url'],
            through: {
              attributes: [],
            },
            include: [
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname', 'email'],
                through: {
                  attributes: [],
                },
              },
            ],
          }],
        });

        await event.addParticipants(args.userId);

        var emailReferents = '';

        event.actors.map((actor) => {

          actor.referents.map((referent) => {
            emailReferents += referent.email + ',';
          });
        });

        const emailTitle =
          "Nouveau participant à l'événement " + event.label;

        const emailTemplate = 'newActionParticipate';
        const user = await User.findByPk(args.userId);

        const data = {
          eventName: event.label,
          idEvent: args.eventId,
          firstNameVolunteer: user.surname,
          lastNameVolunteer: user.lastname,
          emailVolunteer: user.email,
          url: process.env.CLIENT_URL + "/admin/event"
        };

        await sendEmail(process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data); //emailReferents+process.env.EMAIL_CONTACT



        return true;
      } catch (error) {
        console.log(error);
      }
    },
    removeEventParticipate: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({ where: { id: args.eventId } });

        await event.removeParticipants(args.userId);
        return true;
      } catch (error) {
        console.log(error);
      }
    },
    deleteEvent: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id', 'event_id'],
              },
            },
            {
              model: Actor,
              as: 'actors',
              attributes: ['id', 'name','url'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
          ],
        });
        
        // checker si le user est admin d'une étape qui est admin de l'event
        if (context.req?.session?.user?.role !== 'admin') {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Actor,
                as: 'adminactors',
              },
            ]
          });

          const u = user.get({ plain: true});
          const actors = u.adminactors.map(s => s.id);

          if (event.get({ plain: true}).actors.map(s => s.id).every(s => !actors.includes(s))) {
            throw new GraphQLError('Not allowed to delete this event');
          }
        }

        const referentsDeletion = event.referents.map(async (referent) => {
          await event.removeReferents(referent);
        });
        await Promise.all(referentsDeletion);
        const actorsDeletion = event.actors.map(async (actor) => {
          await event.removeActors(actor);
        });
        await Promise.all(actorsDeletion);

        const picturesDeletion = event.pictures.map(async (picture) => {
          await event.removePictures(picture);
        });
        await Promise.all(picturesDeletion);

        const result = await Event.destroy({
          where: {
            id: args.eventId,
          },
        });

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
  Event: {
    /* collecton's author */
  },
};
const uploadImage = async (pictures, eventID) => {
  //upload files
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true
            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToEvent(
                image.file.filename,
                `${eventID}`
              )])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${eventID}-${fileToken}-original`,
                  position: index,
                  eventID: eventID,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    const uploadedFiles = await uploadedFilesPromise;

    return uploadedFiles.map(async (file) => {
      if (!file.newpic && file.deleted) {
        // supprimer une image déja existante

        return new Promise((resolve, reject) => {
          Picture.findByPk(file.id).then((picture) => {
            fs.unlink(
              `/static/images/event/${eventID}/${picture.originalPictureFilename}.jpeg`,
              () => {
                picture.destroy().then((value) => {
                  resolve({
                    id: file.id,
                    destroyed: value,
                  });
                });
              },
            );
          });
        });
      } else {
        // sinon traiter les images à updater

        if (file.newpic) {
          if (file.originalPictureFilename) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setEvent(eventID);
            return result;
          } else {
            return null;
          }

        } else {
          // si l'image est deja existante, il faut updater sa position
          const result = Picture.update(
            file,
            // {
            // position : file.position
            // }
            { where: { id: file.id } },
          );
          // position = position +1;
          return result;
        }
      }
    });

    const result = await Promise.all(_);
    /*  result.map((picture) => {
        if(picture.dataValues){
        picture.dataValues.setActor(actorId)
        }
    });
*/

    //console.log(result);
  }
};

export default composeResolvers(eventResolvers, {
  'Mutation.createEvent': [isAuthenticated()],
  'Mutation.editEvent': [isAuthenticated()],
  'Mutation.deleteEvent': [isAuthenticated()],
});

