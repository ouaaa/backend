import { GraphQLError } from 'graphql';

export const isAuthenticated = () => (next) => (root, args, context, info) => {
  if (!context.req?.session?.user?.id) {
    throw new GraphQLError('You must be authenticated!', {
      extensions: {
        code: 'UNAUTHENTICATED',
        http: { status: 401 },
      },
    });
  }

  return next(root, args, context, info);
};

export const isAdmin = () => (next) => (root, args, context, info) => {
  if (context.req?.session?.user?.role !== 'admin') {
    throw new GraphQLError('You must be admin!', {
      extensions: {
        code: 'UNAUTHENTICATED',
        http: { status: 401 },
      },
    });
  }

  return next(root, args, context, info);
};
