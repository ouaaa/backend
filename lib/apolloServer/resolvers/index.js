import authResolvers from 'lib/apolloServer/resolvers/mutations/authResolvers';
import userInfosResolvers from 'lib/apolloServer/resolvers/mutations/userInfosResolver';
import actorResolvers from 'lib/apolloServer/resolvers/mutations/actorResolver';
import validateEmailResolver from 'lib/apolloServer/resolvers/mutations/validateEmailResolver';
import eventResolvers from 'lib/apolloServer/resolvers/mutations/eventResolvers';
import sendValidationEmailResolver from 'lib/apolloServer/resolvers/mutations/sendValidationEmailResolver';
import newsletterResolvers from 'lib/apolloServer/resolvers/mutations/newsletterResolvers';
import collectionResolvers from 'lib/apolloServer/resolvers/mutations/collectionResolvers';
import contactResolver from 'lib/apolloServer/resolvers/mutations/contactResolver';
import searchResolvers from 'lib/apolloServer/resolvers/mutations/searchResolvers';
import favoriteResolvers from 'lib/apolloServer/resolvers/mutations/favoriteResolvers';
import articleResolvers from 'lib/apolloServer/resolvers/mutations/articleResolvers';
import resourceResolvers from 'lib/apolloServer/resolvers/mutations/resourceResolvers';
import recipeResolvers from 'lib/apolloServer/resolvers/mutations/recipeResolvers';
export default [
  authResolvers,
  userInfosResolvers,
  actorResolvers,
  validateEmailResolver,
  eventResolvers,
  sendValidationEmailResolver,
  newsletterResolvers,
  collectionResolvers,
  contactResolver,
  searchResolvers,
  favoriteResolvers,
  articleResolvers,
  resourceResolvers,
  recipeResolvers,
];