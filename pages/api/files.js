import formidable from "formidable";
import fs from "fs";
import { v4 as uuidv4 } from 'uuid';

export const config = {
  api: {
    bodyParser: false
  }
};

const post = async (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, async function (err, fields, files) {
    if (files.files!== undefined) {
      await saveFile(files.files);
      return res.status(201).send({ url: `/static/images/newUpload/${files.files.name}`});
    } else if (files.upload) {
      const name = await saveFile(files.upload, 'uploads', true);
      return res.status(201).send({ url: `${req.headers.publicurl}/static/images/uploads/${name}`});
    }
  });
};

const saveFile = async (file, folder = 'newUpload', anonymize = false) => {
  const data = fs.readFileSync(file.path);
  const name = anonymize ? uuidv4() : file.name;
  let dirpath = `./public/static/images/${folder}`;
  let path = dirpath + `/${name}`;
  fs.mkdirSync(dirpath, { recursive: true });
  fs.writeFileSync(path, data);
  await fs.unlinkSync(file.path);
  return name;
};

export default (req, res) => {
  req.method === "POST"
    ? post(req, res)
    : req.method === "PUT"
      ? console.log("PUT")
      : req.method === "DELETE"
        ? console.log("DELETE")
        : req.method === "GET"
          ? console.log("GET")
          : res.status(404).send("");
};
