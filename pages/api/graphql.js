import session from 'express-session';
import resolvers from 'lib/apolloServer/resolvers';
import startDB from 'lib/sequelize/models';
import sessionSequelize from 'connect-session-sequelize';
import { Sequelize } from 'sequelize';
import initEmaiLTransporter from 'lib/nodemailer/init';
import { ApolloServer } from "@apollo/server";
import { startServerAndCreateNextHandler } from '@as-integrations/next';
import { typeDefs } from 'lib/apolloServer/schema';
export const emailTransporter = initEmaiLTransporter();

function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }
      return resolve(result);
    });
  });
}

const SequelizeStore = sessionSequelize(session.Store);

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'db/session.sqlite',
  logging: false,
});

const myStore = new SequelizeStore({
  db: sequelize,
});

const sess = session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  name: 'id',
  // rolling : false, // restart maxAge countdown after each requests
  cookie: {
    domain: process.env.COOKIE_DOMAIN,
  },
  store: myStore,
});

myStore.sync();

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  debug: process.env.NODE_ENV === 'production' ? false : true,
  introspection: process.env.NODE_ENV === 'production' ? false : true,
  playground: false,
});

startDB();

const allowedHeaders = [
  "access-control-allow-credentials",
  "access-control-allow-origin",
  "apollographql-client-name", // this might be key for you
  "authorization",
  "content-type",
];

export const allowNextApiCors = (handler) =>
  async (req, res) => {
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Origin", "*");// research having this set to *
    res.setHeader("Access-Control-Allow-Methods", ["GET", "OPTIONS", "POST"]);
    res.setHeader("Access-Control-Allow-Headers", allowedHeaders.join(","));
    if (req.method === "OPTIONS") {
      res.status(200).end();
      return;
    }
    await handler(req, res);
  };

const allowCors = (fn) => async (req, res) => {
  await runMiddleware(req, res, sess);
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('origin', process.env.CLIENT_URL)
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*')
  // another common pattern
  res.setHeader('Access-Control-Allow-Methods', 'GET,OPTIONS,PATCH,DELETE,POST,PUT')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version', 'apollographql-client-version', 'apollographql-client-name',
  )
  if (req.method === 'OPTIONS') {
    res.status(200).end()
    return
  }
  await fn(req, res)
}

const handlers = startServerAndCreateNextHandler(apolloServer, {
  context: async (req, res) => ({ req, res, context: sess }),
});

export default allowCors(handlers);