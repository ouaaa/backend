'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const Article = queryInterface.sequelize.define('Article', null, { tableName: 'article' });
      const articles = await Article.findAll();

      await queryInterface.bulkInsert('resource', articles.map((article) => ({
        article_id: article.id,
        type: 'ARTICLE',
        createdAt: article.createdAt,
        updatedAt: article.updatedAt,
      })));

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.error('Error during migration for articles to resources', error);
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('resource', null, {});
  }
};
