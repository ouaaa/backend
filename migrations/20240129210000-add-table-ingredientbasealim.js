'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('base_alim_ingredient', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      produit: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      poids: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      energie: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      proteines: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      lipides: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      glucides: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      empreinteCarbone: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      agriculture: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      transformation: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      emballage: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      transport: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      distribution: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      consommation: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      poidsParUnite: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      densite: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      poidsParCuillereASoupe: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      poidsParCuillereACafe: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
      },
    });

    // Add foreign key to connect Ingredient to BaseAlimIngredient
    await queryInterface.addColumn('ingredient', 'baseAlimIngredientId', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'base_alim_ingredient',
        key: 'id',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    // Remove the foreign key constraint
    await queryInterface.removeColumn('ingredient', 'baseAlimIngredientId');

    // Drop the table
    await queryInterface.dropTable('base_alim_ingredient');
  },
};
