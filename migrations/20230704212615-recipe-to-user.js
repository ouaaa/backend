'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('recipe', 'user_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'user',
        field: 'id'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.removeColumn('recipe', 'user_id');
  }
};
