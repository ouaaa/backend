'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'event', // name of Source model
      'practicalInfo', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'event', // name of Source model
      'registerLink', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'event', // name of Source model
      'practicalInfo', // key we want to remove
    );
    await queryInterface.removeColumn(
      'event', // name of Source model
      'registerLink', // key we want to remove
    );
  },
};
