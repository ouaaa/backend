module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('actor', 'url', {
            type: Sequelize.STRING,
            allowNull: true,
        });
        await queryInterface.addColumn('event', 'url', {
            type: Sequelize.STRING,
            allowNull: true,
        });
        await queryInterface.addColumn('article', 'url', {
            type: Sequelize.STRING,
            allowNull: true,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('actor', 'url');
        await queryInterface.removeColumn('event', 'url');
        await queryInterface.removeColumn('article', 'url');
    },
};
