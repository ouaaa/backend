'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'collections', // name of Source model
      'multipleSelection', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'collections', // name of Source model
      'withDescription', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'collections', // name of Source model
      'position', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'multipleSelection', // key we want to remove
    );
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'withDescription', // key we want to remove
    );
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'position', // key we want to remove
    );
  },
};
