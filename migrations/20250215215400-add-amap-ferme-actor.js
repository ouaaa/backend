'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('actor', 'amapContratLegumes', Sequelize.STRING);
    await queryInterface.addColumn('actor', 'amapAutresContrats', Sequelize.STRING);
    await queryInterface.addColumn('actor', 'fermeModeDistribution', Sequelize.STRING);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('actor', 'amapContratLegumes');
    await queryInterface.removeColumn('actor', 'amapAutresContrats');
    await queryInterface.removeColumn('actor', 'fermeModeDistribution');
  }
};
