'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'collections', // name of Source model
      'event', // name of the key we're adding
      {
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'event', // key we want to remove
    );
    await queryInterface.removeConstraint(
      'entries_actors',
      'Entries_collection_id_foreign_idx',
    );
    await queryInterface.dropTable('entries_actors');
  },
};
