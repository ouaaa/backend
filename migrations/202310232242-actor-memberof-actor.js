'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('actor_memberof_actor', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      actormemberof_id: {
        type: Sequelize.INTEGER,
        references: { model: 'actor', key: 'id' },
        onDelete: 'CASCADE',
      },
      actor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'actor', key: 'id' },
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      'actor_memberof_actor'
    );
    await queryInterface.dropTable('actor_memberof_actor');
  },
};
