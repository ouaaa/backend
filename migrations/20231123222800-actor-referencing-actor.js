'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('actor', 'referencing_actor_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'actor',
        field: 'id'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.removeColumn('actor', 'referencing_actor_id');
  }
};
