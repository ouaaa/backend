'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'inviteActor', // name of Source model
      'contact_name', // name of the key we're adding
      {
        type: Sequelize.STRING,
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'inviteActor', // name of Source model
      'contact_name', // key we want to remove
    );
  },
};
