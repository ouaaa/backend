'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('resource', 'type', Sequelize.ENUM('ARTICLE', 'ARTICLE_OUAAA', 'RECIPE','VIDEO_ACTOR'));
    await queryInterface.addColumn(
      'resource','actor_id',{
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'actor',
            field: 'id'
          }
      }
    );
    },
    
  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('resource', 'type', Sequelize.ENUM('ARTICLE', 'ARTICLE_OUAAA', 'RECIPE'));
    queryInterface.removeColumn(
      'resource', // name of Source model
      'actor_id', // key we want to remove
    );
  }
};
