'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'entries', // name of Source model
      'color', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'entries', // name of Source model
      'icon', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'entries', // name of Source model
      'description', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'entries', // name of Source model
      'color', // key we want to remove
    );
    await queryInterface.removeColumn(
      'entries', // name of Source model
      'icon', // key we want to remove
    );
    await queryInterface.removeColumn(
      'entries', // name of Source model
      'description', // key we want to remove
    );
  },
};
