const { unique } = require("next/dist/build/utils");

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('recipe', 'url', {
            type: Sequelize.STRING,
            allowNull: true,
            unique: true,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('recipe', 'url');
    },
};
