'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('recipe', 'actor_id', {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'actor',
        field: 'id'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.removeColumn('recipe', 'actor_id');
  }
};
