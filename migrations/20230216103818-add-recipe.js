'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'recipe',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        label: {
          type: Sequelize.STRING,
        },
        shortDescription: {
          type: Sequelize.STRING,
        },
        content: {
          type: Sequelize.TEXT,
        },
        time: {
          type: Sequelize.INTEGER,
        },
        nbPerson: {
          type: Sequelize.INTEGER,
        },
        type: {
          type: Sequelize.STRING,
        }
      }
    );

    await queryInterface.createTable(
      'recipe_tag',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        label: {
          allowNull: false,
          type: Sequelize.STRING,
        },
      }
    );

    await queryInterface.createTable(
      'recipe_has_tag',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        recipe_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'recipe',
            key: 'id',
          },
          onDelete: 'CASCADE'
        },
        recipe_tag_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'recipe_tag',
            key: 'id',
          },
          onDelete: 'CASCADE'
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }
    );

    await queryInterface.createTable(
      'resource',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        type: {
          type: Sequelize.ENUM('ARTICLE', 'ARTICLE_OUAAA', 'RECIPE'),
        },
        article_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'article',
            field: 'id'
          }
        },
        recipe_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'recipe',
            field: 'id'
          },
        },
      }
    );

    

    return queryInterface.addColumn(
      'picture', // name of Source model
      'recipe_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'recipe', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'RESTRICT',
        onDelete: 'RESTRICT',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeConstraint('recipe', 'picture_recipe_id_foreign_idx');
    queryInterface.removeColumn(
      'picture', // name of Source model
      'recipe_id', // key we want to remove
    );


    await queryInterface.dropTable('resource');
    await queryInterface.dropTable('recipe_has_tag');
    await queryInterface.dropTable('recipe_tag');
    await queryInterface.dropTable('recipe');
  }
};
