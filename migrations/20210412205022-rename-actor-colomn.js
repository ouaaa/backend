'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.renameColumn(
      'actor',
      'short_description',
      'shortDescription',
    );
    queryInterface.renameColumn(
      'event',
      'short_description',
      'shortDescription',
    );
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.renameColumn(
      'actor',
      'shortDescription',
      'short_description',
    );
    queryInterface.renameColumn(
      'event',
      'shortDescription',
      'short_description',
    );
  },
};
