'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'ingredient',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        name: {
          type: Sequelize.STRING,
        },
        unit: {
          type: Sequelize.STRING,
        },
        impact: {
          type: Sequelize.INTEGER,
        },
        description: {
          type: Sequelize.STRING
        },
      }
    );

    return await queryInterface.createTable(
      'recipe_ingredient',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        recipe_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'recipe',
            key: 'id',
          },
          onDelete: 'CASCADE'
        },
        recipe_ingredient_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'ingredient',
            key: 'id',
          },
          onDelete: 'CASCADE'
        },
        quantity: {
          type: Sequelize.STRING,
        },
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('recipe_ingredient');
    return await queryInterface.dropTable('ingredient');
  }
};
