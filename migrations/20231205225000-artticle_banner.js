'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('actor', 'bannerPrincipalPicture', {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true,
    });
    await queryInterface.addColumn('article', 'bannerPrincipalPicture', {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true,
    });
    await queryInterface.addColumn('event', 'bannerPrincipalPicture', {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true,
    });
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.removeColumn('actor', 'bannerPrincipalPicture');
     await queryInterface.removeColumn('article', 'bannerPrincipalPicture');
     await queryInterface.removeColumn('event', 'bannerPrincipalPicture');
  }
};
