'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'entries', // name of Source model
      'collection_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'collections', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'entries', // name of Source model
      'collection_id', // key we want to remove
    );
  },
};
