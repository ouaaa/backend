'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'actor', // name of Source model
      'socialNetwork', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'collections', // name of Source model
      'filter', // name of the key we're adding
      {
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'collections', // name of Source model
      'actor', // name of the key we're adding
      {
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'actor', // name of Source model
      'socialNetwork', // key we want to remove
    );
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'filter', // key we want to remove
    );
    await queryInterface.removeColumn(
      'collections', // name of Source model
      'actor', // key we want to remove
    );
  },
};
