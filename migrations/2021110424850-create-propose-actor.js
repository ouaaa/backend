'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('inviteActor', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      requesterName: {
        type: Sequelize.STRING,
      },
      actorName: {
        type: Sequelize.STRING,
      },
      actorEmail: {
        type: Sequelize.STRING,
      },
      postCode: {
        type: Sequelize.STRING,
      },
      message: {
        type: Sequelize.STRING,
      },
      sendEmail: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('inviteActor');
  },
};
