'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('entries_actors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      actor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'actor', key: 'id' },
        onDelete: 'CASCADE',
      },
      entry_id: {
        type: Sequelize.INTEGER,
        references: { model: 'entries', key: 'id' },
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      'entries_actors',
      'Entries_collection_id_foreign_idx',
    );
    await queryInterface.dropTable('entries_actors');
  },
};
