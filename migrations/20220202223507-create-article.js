'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('article', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      label: {
        type: Sequelize.STRING,
      },
      content: {
        type: Sequelize.STRING,
      },
      shortDescription: {
        type: Sequelize.STRING,
      },
      isValidated: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.createTable('actor_article', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      actor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'actor', key: 'id' },
        onDelete: 'CASCADE',
      },
      article_id: {
        type: Sequelize.INTEGER,
        references: { model: 'article', key: 'id' },
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    return queryInterface.addColumn(
      'picture', // name of Source model
      'article_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'article', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'RESTRICT',
        onDelete: 'RESTRICT',
      },
    );
  },
  down: async (queryInterface, Sequelize) => {
     queryInterface.removeColumn(
      'picture', // name of Source model
      'article_id', // key we want to remove
    );
    await queryInterface.dropTable('actor_article');
    return await queryInterface.dropTable('article');
    
  },
};
