'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.renameTable(
      'actors_entries',
      'actorEntries',
    );
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.renameTable(
      'actorEntries',
      'actors_entries',
    );
  },
};
