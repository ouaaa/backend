'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'picture', // name of Source model
      'logo', // name of the key we're adding
      {
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'picture', // name of Source model
      'main', // name of the key we're adding
      {
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    await queryInterface.addColumn(
      'actor', // name of Source model
      'volunteerDescription', // name of the key we're adding
      {
        type: Sequelize.STRING,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    return queryInterface.addColumn(
      'actor', // name of Source model
      'contact_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'user', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'picture', // name of Source model
      'logo', // key we want to remove
    );
    await queryInterface.removeColumn(
      'picture', // name of Source model
      'main', // key we want to remove
    );
    await queryInterface.removeColumn(
      'actor', // name of Source model
      'contact_id', // key we want to remove
    );
    await queryInterface.removeColumn(
      'actor', // name of Source model
      'volunteerDescription', // key we want to remove
    );
  },
};
