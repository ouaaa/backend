'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'picture', // name of Source model
      'product_id', // key we want to remove
    );
    await queryInterface.dropTable('actorsentries');
    await queryInterface.dropTable('actor_categories');
    await queryInterface.dropTable('cart');
    await queryInterface.dropTable('cart_product');
    await queryInterface.dropTable('event_categories');
    await queryInterface.dropTable('category');
    await queryInterface.dropTable('code_controls');

    await queryInterface.dropTable('order');
    await queryInterface.dropTable('order_product');
    await queryInterface.dropTable('product');
    await queryInterface.dropTable('product_category');
    await queryInterface.dropTable('sale_actions');
    await queryInterface.dropTable('user_product');
    await queryInterface.dropTable('entries_actors');

    
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('actorsentries');
  },
};
