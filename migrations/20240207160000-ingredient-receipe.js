'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('ingredient', 'recipe_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'recipe',
        field: 'id'
      },
    });

    await queryInterface.addColumn('ingredient', 'quantity', {
      type: Sequelize.FLOAT,
      allowNull: true,

    });
  },

  down: async (queryInterface, Sequelize) => {
    return await queryInterface.removeColumn('ingredient', 'recipe_id');
    return await queryInterface.removeColumn('ingredient', 'quantity');
  }
};
