'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'event', // name of Source model
      'event_id', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'event', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'event', // name of Source model
      'event_id', // key we want to remove
    );
  },
};
