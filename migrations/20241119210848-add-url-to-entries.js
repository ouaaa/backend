'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('entries', 'url', {
      type: Sequelize.STRING,
      allowNull: true,  // You can adjust this depending on whether the column should be nullable
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('entries', 'url');
  }
};