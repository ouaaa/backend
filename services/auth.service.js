import randToken from 'rand-token';
import { NO_AUTH, USER, ADMIN } from 'config/constants/constants';

const issueRefreshToken = () => {
  return randToken.uid(256);
};

const issueEmailValidationToken = () => {
  return randToken.uid(20);
};

const verifyAuthentification = (context, auth) => {
  if (!auth) throw new Error(errorName.GLOBAL_AUTH_ERROR);
  if (auth === NO_AUTH) return true;
  else {
    if (auth === USER) {
      return verifyUserAuthentification(context);
    } else {
      if (auth === ADMIN) {
        return verifyAdminAuthentification(context);
      } else throw new Error(errorName.GLOBAL_AUTH_ERROR);
    }
  }
};

const verifyUserAuthentification = (context) => {
  if (context.user) {
    if (context.user.role == 'user' || context.user.role == 'admin') {
      return true;
    } else {
      throw new Error(errorName.USER_AUTH_NEEDED);
    }
  } else {
    throw new Error(errorName.USER_AUTH_NEEDED);
  }
};

const verifyAdminAuthentification = (context) => {
  if (context.user && context.user.role == 'admin') {
    return true;
  } else {
    throw new Error(errorName.ADMIN_AUTH_NEEDED);
  }
};

export {
  issueRefreshToken,
  issueEmailValidationToken,
  verifyAuthentification,
  verifyUserAuthentification,
  verifyAdminAuthentification,
};
