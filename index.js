import session from 'express-session';
import express from 'express';
import resolvers from './lib/apolloServer/resolvers/index.js';
import { startStandaloneServer } from '@apollo/server/standalone';
import startDB from './lib/sequelize/models.js';
import sessionSequelize from 'connect-session-sequelize';
import { Sequelize } from 'sequelize';
import initEmaiLTransporter from './lib/nodemailer/init.js';
import { gql } from 'graphql-tag'
import { ApolloServer } from "@apollo/server";
import { startServerAndCreateNextHandler } from '@as-integrations/next';
import { typeDefs } from './lib/apolloServer/schema.js';
import { ApolloServerPluginLandingPageLocalDefault, ApolloServerPluginLandingPageProductionDefault } from '@apollo/server/plugin/landingPage/default';
import cors from 'cors';
export const emailTransporter = initEmaiLTransporter();

function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }
      return resolve(result);
    });
  });
}

var SequelizeStore = sessionSequelize(session.Store);

var sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'db/session.sqlite',
  logging: false,
});

const myStore = new SequelizeStore({
  db: sequelize,
});

const sess = session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  name: 'id',
  // rolling : false, // restart maxAge countdown after each requests
  cookie: {
    domain: process.env.COOKIE_DOMAIN,
  },
  store: myStore,
});

myStore.sync();




const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
    debug: process.env.NODE_ENV === 'production' ? false : true,
    introspection: process.env.NODE_ENV === 'production' ? false : true,
    playground: true,
    context: ({ req }) => {
        const session = req.session || null;
        return { session };
    },
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()]
});

// Generate method to create graphql server
// const createServer = () => {
//   return new GraphQLServer({
//     typeDefs: 'src/schema.graphql',
//     resolvers,
//     resolverValidationOptions: {
//       requireResolversForResolveType: false,
//     },
//     context: req => ({ ...req, db }),
//   });

// };


startDB();


const allowCors = (fn) => async (req, res) => {
  await runMiddleware(req, res, sess);
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('origin', 'http://localhost:3000')
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*')
  // another common pattern
  res.setHeader('Access-Control-Allow-Methods', 'GET,OPTIONS,PATCH,DELETE,POST,PUT')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version','apollographql-client-version', 'apollographql-client-name',
  )
  if (req.method === 'OPTIONS') {
    res.status(200).end()
    return
  }
  await fn(req, res)
}


async function handler(req, res) {
  await runMiddleware(req, res, sess);

  if (process.env.NODE_ENV == 'production') {
    // to add to conf
    const whitelist = [
      'https://app.acteursdelatransition.fr',
      'https://api.app.acteursdelatransition.fr',
      'https://ouaaa-transition.fr',
      'https://recette.ouaaa-transition.fr',
      'https://test.ouaaa-transition.fr',
      'http://51.68.84.238:18000',
    ];
    await runMiddleware(
      req,
      res,
      cors({
        credentials: true,
        origin: whitelist,
      }),
    );
  } else {
    const whitelist = ['http://localhost:3000', 'http://127.0.0.1:3000',,'http://192.168.80.69:3000'];

    await runMiddleware(
      req,
      res,
      cors({
        credentials: true,
        origin: whitelist,
      }),
    );
  }

  return startServerAndCreateNextHandler(apolloServer, {
    context: async (req, res) => ({ req, res }),
  })
}


/*const handlers = startServerAndCreateNextHandler(apolloServer, {
  context: async (req, res) => ({ req, res, session:sess}),
});*/
const { url } = await startStandaloneServer(apolloServer, {
  listen: { port: 4000 },
});

//export default allowCors(handlers)
