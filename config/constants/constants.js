/* GraphQL auth constants*/
export const NO_AUTH = 'NO_AUTH';
export const USER = 'USER';
export const ADMIN = 'ADMIN';

/* Mutation auth constants */
export const EMAIL_VALIDATION_AUTH = NO_AUTH;
export const SEND_EMAIL_VALIDATION_AUTH = NO_AUTH;
