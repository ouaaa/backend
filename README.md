# Start in local

Make sure you have docker installed
Run `docker-compose up -d` to start the mysql database and phpmyadmin

Duplicate .env.dist file, and rename the copy to .env.development (will be used by process.env)

`npm install` -- to fetch dependancies

`npm run dev` -- launch server on localhost:8083

You can verify if the server is launched with this url :
Graphql api in browser : http://localhost:8083/api/graphql

## Seed the database

Make sure you have sequelize CLI installed, otherwise run `npm i -g sequelize-cli`
Run `sequelize db:seed:all`

## Run migrations

Make sure you have sequelize CLI installed, otherwise run `npm i -g sequelize-cli`
Run `sequelize db:migrate`

## Clear the database
Make sure you have sequelize CLI installed, otherwise run `npm i -g sequelize-cli`
Run `sequelize db:seed:undo:all`

## Environnement

```
node >= 20
npm >= 10
```