# build environment
FROM node:21.6.0 as build

# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /app

ARG EMAIL_FROM
ENV EMAIL_FROM=$EMAIL_FROM
ARG EMAIL_HOST
ENV EMAIL_HOST=$EMAIL_HOST
ARG EMAIL_PASSWORD
ENV EMAIL_PASSWORD=$EMAIL_PASSWORD
ARG EMAIL_PORT
ENV EMAIL_PORT=$EMAIL_PORT
ARG EMAIL_USER
ENV EMAIL_USER=$EMAIL_USER
ARG SESSION_SECRET
ENV SESSION_SECRET=$SESSION_SECRET

ENV PATH /app/node_modules/.bin:$PATH
# Installing dependencies
COPY package*.json ./
RUN npm install  --force

# Copying source files
COPY . ./

# RUN npx sequelize  --env production db:migrate

# Building app
RUN npm run build

# Running the app
CMD [ "npm", "start" ]